/*
 * imaginary_time_evolution_honeycomb.cpp
 *
 *  Created on: 2014��1��28��
 *      Author: ZhaoHuihai
 */

#include <iostream>
//-----------------------------
// intel mkl
#include <mkl.h>

#include "parameter.h"
#include "operators.h"
#include "environment_honeycomb.h"
#include "wave_function.h"
#include "imaginary_time_evolution_honeycomb.h"
#include "KitaevHeisenberg.h"

using namespace std ;

ImaginaryTimeEvolutionHoneycomb::ImaginaryTimeEvolutionHoneycomb()
{
	method = - 1 ;

	gaugeFix = false ;
	preCond = false ;
	minEig = 0 ;

	disp = false ;

	rightProjectors = TensorArray<double>(0) ;
	leftProjectors = TensorArray<double>(0) ;
}

ImaginaryTimeEvolutionHoneycomb::ImaginaryTimeEvolutionHoneycomb(
		Parameter parameter, WaveFunction &wave)
{
	method = - 1 ;

	gaugeFix = parameter.ITE_gaugeFix ;
	preCond = parameter.ITE_preCond ;
	minEig = parameter.ITE_minEig ;
	if ( minEig >= 1)
	{
		cout << "error: min eig value must be small than 1." << endl ;
		exit(0) ;
	}

	disp = parameter.disp ;

	initializeProjector(parameter) ;
	//--------------------------------------------------------------------------

	if (parameter.ITE)
	{
		cout << "apply first order Trotter decomposition " << endl ;
		findGroundStateWave(parameter, wave) ;
	}
}

ImaginaryTimeEvolutionHoneycomb::~ImaginaryTimeEvolutionHoneycomb() { }

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void ImaginaryTimeEvolutionHoneycomb::initializeProjector(Parameter &parameter)
{
	int numOfPro = parameter.TNS_subLatNo * 3 / 2 ;

	rightProjectors = TensorArray<double>(numOfPro) ;
	leftProjectors = TensorArray<double>(numOfPro) ;

	int M = parameter.siteDim ;
	int D = parameter.TNS_bondDim ;

	for ( int i = 0; i < numOfPro; i ++ )
	{
		rightProjectors(i) = Tensor<double>(D * M * M, D) ;
		rightProjectors(i).randUniform() ;

		leftProjectors(i) = Tensor<double>(D * M * M, D) ;
		leftProjectors(i).randUniform() ;
	}
}

void ImaginaryTimeEvolutionHoneycomb::findGroundStateWave(Parameter &parameter, WaveFunction &wave)
{
	if (parameter.ITE_loadPreviousWave)
	{
		if (parameter.ITE_polarization)
		{
			cout << "warning: Old wave is loaded, unnecessary to polarize" << endl ;
			cout << "The evolution process will NOT polarize the wave function" << endl ;
			parameter.ITE_polarization = false ;
		}
		//********************************************************

		if ( wave.fullStep > 0 )
		{
			cout << "the wave function is updated by full update" << endl ;
			cout << "simple update will be NOT used in evolution" << endl ;
			parameter.ITE_simpleUpdate = false ;
		}

	}
	//-------------------------------------------------------------------------------------

	// use simple update to polarize
	if (parameter.ITE_polarization)
	{
		polarizeWave(parameter, wave) ;
		wave.polarization = true ;
	}

	if (parameter.ITE_simpleUpdate)
	{
		cout << "simple update" << endl ;
		method = 1 ;
		if ( wave.simpleStep > 0 )
		{
			cout << "warning: simpleStep != 0, simple update only at tauFinal." << endl ;
			parameter.ITE_tauInitial_simple = parameter.ITE_tauFinal_simple ;
		}

		applyWaveProjection_simple(parameter, wave) ;
	}

	if (parameter.ITE_fullupdate)
	{
		cout << "full update by variational approach." << endl ;
		method = 1 ;
		if (wave.fullStep > 0)
		{
			cout << "warning: fullStep != 0, full update only at tauFinal." << endl ;
			parameter.ITE_tauInitial_full = parameter.ITE_tauFinal_full ;
		}
		applyWaveProjection_full(parameter, wave) ;
	}
}

void ImaginaryTimeEvolutionHoneycomb::polarizeWave(Parameter parameter, WaveFunction &wave)
{
	cout << " Polarize the wave function by simple update " << endl ;
	method = 1 ;

	parameter.ITE_tauInitial_simple = parameter.ITE_tauInitial_polar ;
	parameter.ITE_tauFinal_simple = parameter.ITE_tauFinal_polar ;
	parameter.ITE_tauReduceStep_simple = parameter.ITE_tauReduceStep_polar ;
	parameter.ITE_tauFinalStep_simple = parameter.ITE_tauFinalStep_polar ;

	parameter.field_z = parameter.ITE_polarField_z ;

	applyWaveProjection_simple(parameter, wave) ;

	// reset simple update steps to 0
	wave.simpleStep = 0 ;

}

void ImaginaryTimeEvolutionHoneycomb::applyWaveProjection_simple(Parameter parameter, WaveFunction &wave)
{

	cout << "site dimension = " << parameter.siteDim << endl ;
	cout << "wave function bond dimension = " << parameter.TNS_bondDim << endl ;

	double tau = parameter.ITE_tauInitial_simple ;
	double tauFinal = parameter.ITE_tauFinal_simple ;
	int tauReduceStep = parameter.ITE_tauReduceStep_simple ;
	int tauFinalStep = parameter.ITE_tauFinalStep_simple ;
	double tauReduceRatio = pow( 0.1,  1.0 / double(tauReduceStep - 1)) ;
	cout << "tau reduce ratio: " << tauReduceRatio << endl ;

	int j = wave.simpleStep ;
	// create polarized hamiltonian
	// 3 terms
	TensorArray<double> hamiltonian = createHamiltonian(parameter) ;

//	hamiltonian.display() ;
//	exit(0) ;
//	double tau0 ;
	while ( (tau >= tauFinal) )
	{
		cout << "tau = " << tau << endl ;
		//-------------------------------------------------------------------
		j ++ ;

		double truncationError ;

		double t_start = dsecnd() ;
		applyFirstTrotter_simple(parameter, hamiltonian, tau, wave, truncationError) ;
		double t_end = dsecnd() ;
		cout << "simple update step No. " << j << ", tau = " << tau << endl ;
		cout << "truncation error = " << truncationError << endl ;
		cout << "simple update time: " << (t_end - t_start) << " s" << endl ;
		//-------------------------------------------------------------------
//		tau = tau * tauReduceRatio + 1e-15 ;

		tau = tau * tauReduceRatio ;
		if ( (tau < tauFinal) && (tauFinalStep > 1) )
		{
			tau = tauFinal ;
			tauFinalStep -- ;
		}
	}

	cout << "total simple update steps: " << j << endl ;

	wave.simpleStep = j ;
	wave.tau = tau ;
	wave.save() ;
	cout << "***********************************************************************************" << endl ;
}

TensorArray<double> ImaginaryTimeEvolutionHoneycomb::createHamiltonian(Parameter &parameter)
{
	TensorArray<double> hamiltonian(3) ;

	Operators operators(parameter) ;
	// create polarized hamiltonian
	hamiltonian = operators.createHamiltonian_Kitaev() ;

	return hamiltonian ;
}

void ImaginaryTimeEvolutionHoneycomb::applyFirstTrotter_simple(Parameter parameter,
		TensorArray<double> hamiltonian, double tau, WaveFunction &wave, double &truncationError)
{
	Tensor<double> truncErr_single(3) ;

	for (int i = 0; i < 3; i ++)
	{
		simpleUpdate(parameter, hamiltonian(i), tau, wave, truncErr_single(i),
				rightProjectors(i), leftProjectors(i)) ;
		wave.rotate() ;
	}

	truncationError = truncErr_single.max() ;
}

//***********************************************************************************

// Tensors to update: wave.A(0), wave.A(1), wave.Lambda(0,0), wave.Lambda(1,0), PR, PL
void ImaginaryTimeEvolutionHoneycomb::simpleUpdate(Parameter parameter,
		Tensor<double> hamiltonian, double tau, WaveFunction &wave,
		double &truncErr, Tensor<double> &PR, Tensor<double> &PL)
{
	TensorArray<double> PO = createProjectionOperator(parameter, hamiltonian, tau) ;

	//--------------------------------------------------------------------------
	Tensor<double> T0, T1 ;
	// T0(z',x0,y0,m0) T1(z',x1,y1,m1)
	computeProjection(PO, wave.A(0), wave.A(1), T0, T1) ;
	//--------------------------------------------------------------------------
	// step A: absorb lambdas
	// M0((x0,y0,m0),z') = T0(z',x0,y0,m0)*sqrt(L(0,1)(x0) * L(0,2)(y0))
	Tensor<double> M0 = computeT_sqrtLxLy(T0, wave.Lambda(0, 1), wave.Lambda(0, 2)) ;
	// M1((x1,y1,m1),z') = T1(z',x1,y1,m1)*sqrt(L(1,1)(x1) * L(1,2)(y1))
	Tensor<double> M1 = computeT_sqrtLxLy(T1, wave.Lambda(1, 1), wave.Lambda(1, 2)) ;

	//-------------------------------------------------------------------------

	// step B: QR factorization
	// M0((x0,y0,m0),z') = sum{l}_[Q0((x0,y0,m0),l)*R0(l,z')]
	Tensor<double> R0 ;

	qr(M0, R0) ;

	R0 = R0 / R0.norm('1') ;

	// M1((x1,y1,m1),z') = sum{r}_[Q1((x1,y1,m1),r)*R1(r,z')]
	Tensor<double> R1 ;
	qr(M1, R1) ;
	R1 = R1 / R1.norm('1') ;

	//-------------------------------------------------------------------------

	// step C: SVD and truncation
	// M(l,r) = sum{z'}_[R0(l,z')*R1(r,z')]
	Tensor<double> M = contractTensors(R0, 1, R1, 1) ;

	// M(l,r) = sum{z}_[U(l,z)*S(z)*V(r,z)]
	Tensor<double> U, S, V;
	svd_qr(M, U, S, V) ;
//	S.display() ;

	S = S / S(0) ;

	//
	truncateSVD(parameter, U, S, V, truncErr) ;

	Tensor<double> sqrtS = S.sqrt() ;
	//-------------------------------------------------------------------------
	// step D: Create Projector
	// PR(z',z) = sum{r}_[R1(r,z')*V(r,z)/sqrtS(z)]
	PR = createProjector(R1, V, sqrtS) ;

	// PL(z',z) = sum{l}_[R0(l,z')*U(l,z)/sqrtS(z)]
	PL = createProjector(R0, U, sqrtS) ;

	//-------------------------------------------------------------------------
	//  T1(z',x1,y1,m1)
	// step E. Form new system sites: wave.A(0), wave.A(1), wave.Lambda(0,0), wave.Lambda(1,0)
	// A0(z,x0,y0,m0) = sum{z'}_[PR(z',z) * T0(z',x0,y0,m0)]
	wave.A(0) = contractTensors(PR, 0, T0, 0) ;

	// A1(z,x1,y1,m1) = sum{z'}_[PL(z',z) * T1(z',x1,y1,m1)]
	wave.A(1) = contractTensors(PL, 0, T1, 0) ;

	wave.Lambda(0, 0) = S ;
	wave.Lambda(1, 0) = S ;

}
//************************************************************************************

// input arguments: parameter.siteDim
// output arguments: projectionOperator(0)(m0,n0,l) projectionOperator(1)(m1,n1,l)
TensorArray<double> ImaginaryTimeEvolutionHoneycomb::createProjectionOperator(
		Parameter parameter, Tensor<double> hamiltonian, double tau)
{
	int M = parameter.siteDim ;
	int MM = M * M ;

	Tensor<double> V(MM, MM) ;
	Tensor<double> D(MM) ;
	// hamiltonian((m1, m2), (n1, n2)) = V * D * V'
	symEig(hamiltonian, V, D) ;

	D = (- tau) * D ;
	// expD(l)
	Tensor<double> expD = D.exp() ;

	Tensor<double> VD = V ;
	// VD((m1,m2),l) = V((m1,m2),l) * expD(l)
	VD = absorbVector(V, 1, expD) ;

	// PO((m1,m2),(n1,n2))
	Tensor<double> PO = contractTensors(VD, 1, V, 1) ;

	PO.setSmalltoZero(1e-13) ;
	//------------------------------------------------------------
	// PO(m1,m2,n1,n2)
	PO = PO.reshape(M, M, M, M) ;

	// PO(m1,m2,n1,n2) -> PO(m1,n1,m2,n2)
	PO = PO.permute(0, 2, 1, 3) ;

	// PO((m1,n1),(m2,n2))
	PO = PO.reshape(MM, MM) ;
	// PO = U * S * V'
	Tensor<double> U(MM, MM) ;
	Tensor<double> S(MM) ;
	V = Tensor<double>(MM, MM) ;
	svd_qr(PO, U, S, V) ;

	TensorArray<double> projectionOperator(2) ;

	Tensor<double> sqrtS = S.sqrt() ;
	//**********************
	// U((m1,n1),l) = U((m1,n1),l) * sqrtS(l)
	U = absorbVector(U, 1, sqrtS) ;

	projectionOperator(0) = U.reshape(M, M, MM) ; // (m0,n0,l)
	//********************
	// V((m2,n2),l) = V((m2,n2),l) * sqrtS(l)
	V = absorbVector(V, 1, sqrtS) ;

	projectionOperator(1) = V.reshape(M, M, MM) ; // (m1,n1,l)
	//************************
	return projectionOperator ;

}

//************************************************************************************

// T0((z,l),x0,y0,m0) T1((z,l),x1,y1,m1)
void ImaginaryTimeEvolutionHoneycomb::computeProjection(TensorArray<double> PO,
		Tensor<double> A0, Tensor<double> A1, Tensor<double>& T0, Tensor<double>& T1)
{
	Index m0, n0, m1, n1, l ; // PO(0)(m0,n0,l) PO(1)(m1,n1,l)
	Index z, x0, y0, x1, y1 ;
	PO(0).putIndex(m0, n0, l) ;
	PO(1).putIndex(m1, n1, l) ;
	A0.putIndex(z, x0, y0, n0) ;
	A1.putIndex(z, x1, y1, n1) ;
	// T0(z,x0,y0,m0,l) = A0(z, x0, y0, n0) * PO(0)(m0,n0,l)
	T0 = contractTensors(A0, PO(0)) ;
	// T0(z,l,x0,y0,m0)
	T0 = T0.permute(z, l, x0, y0, m0) ;

	// T0(z,l,x0,y0,m0) -> T0((z,l),x0,y0,m0)
	T0 = T0.reshape(Index(z, l), x0, y0, m0) ;
	T0 = T0 / T0.maxAbs() ;

	// T1(z,x1,y1,m1,l) = A1(z, x1, y1, n1) * PO(1)(m1,n1,l)
	T1 = contractTensors(A1, PO(1)) ;
	// T1(z,l,x1,y1,m1)
	T1 = T1.permute(z, l, x1, y1, m1) ;
	// T1(z,l,x1,y1,m1) -> T1((z,l),x1,y1,m1)
	T1 = T1.reshape(Index(z, l), x1, y1, m1) ;
	T1 = T1 / T1.maxAbs() ;
}
//*************************************************************************************

// T((x,y,m),z) = T(z,x,y,m)*sqrt(Lx(x) * Ly(y))
Tensor<double> ImaginaryTimeEvolutionHoneycomb::computeT_sqrtLxLy(
		Tensor<double> T, Tensor<double> Lx, Tensor<double> Ly)
{
	int dz = T.dimension(0) ;
	int dx = T.dimension(1) ;
	int dy = T.dimension(2) ;
	int dm = T.dimension(3) ;

	Tensor<double> sqrtL = Lx.sqrt() ;
	// T(z,x,y,m) = T(z,x,y,m) * sqrt(Lx(x))
	T = absorbVector(T, 1, sqrtL) ;
	//---------------------------------------

	sqrtL = Ly.sqrt() ;
	// T(z,x,y,m) = T(z,x,y,m) * sqrt(Ly(y))
	T = absorbVector(T, 2, sqrtL) ;

	// T(z,x,y,m) -> T(x,y,m,z)
	T = T.permute(1, 2, 3, 0) ;
//	T.info() ;
	// T(x,y,m,z) -> T((x,y,m),z)
	T = T.reshape(dx * dy * dm, dz) ;

	return T ;
}

// T((z,y,m),x) = T(z,x,y,m)*sqrt(Lz(z)*Ly(y))
Tensor<double> ImaginaryTimeEvolutionHoneycomb::computeT_sqrtLzLy(
		Tensor<double> T, Tensor<double> Lz, Tensor<double> Ly)
{
	int dz = T.dimension(0) ;
	int dx = T.dimension(1) ;
	int dy = T.dimension(2) ;
	int dm = T.dimension(3) ;

	Tensor<double> sqrtL = Lz.sqrt() ;
	// T(z,x,y,m) = T(z,x,y,m) * sqrt(Lz(z))
	T = absorbVector(T, 0, sqrtL) ;
	//----------------------------------------

	sqrtL = Ly.sqrt() ;
	// T(z,x,y,m) = T(z,x,y,m) * sqrt(Ly(y))
	T = absorbVector(T, 2, sqrtL) ;

	// T(z,x,y,m) -> T(z,y,m,x)
	T = T.permute(0, 2, 3, 1) ;

	// T(z,y,m,x) -> T((z,y,m),x)
	T = T.reshape(dz * dy * dm, dx) ;

	return T ;
}

// T((z,x,m),y) = T(z,x,y,m)*sqrt(Lz(z) * Lx(x))
Tensor<double> ImaginaryTimeEvolutionHoneycomb::computeT_sqrtLzLx(
		Tensor<double> T, Tensor<double> Lz, Tensor<double> Lx)
{
	int dz = T.dimension(0) ;
	int dx = T.dimension(1) ;
	int dy = T.dimension(2) ;
	int dm = T.dimension(3) ;

	Tensor<double> sqrtL = Lz.sqrt() ;
	// T(z,x,y,m) = T(z,x,y,m)*sqrt(Lz(z))
	T = absorbVector(T, 0, sqrtL) ;
	//----------------------------------------

	sqrtL = Lx.sqrt() ;
	// T(z,x,y,m) = T(z,x,y,m)*sqrt(Lx(x))
	T = absorbVector(T, 1, sqrtL) ;

	// T(z,x,y,m) -> T(z,x,m,y)
	T = T.permute(0, 1, 3, 2) ;

	// T(z,x,m,y) -> T((z,x,m),y)
	T = T.reshape(dz * dx * dm, dy) ;

	return T ;
}

//**************************************************************************************

void ImaginaryTimeEvolutionHoneycomb::truncateSVD(Parameter &parameter,
		Tensor<double>& U, Tensor<double>& S, Tensor<double>& V, double& truncationError)
{
	int Dcut = getDcut(parameter, S) ;

	if ( Dcut < S.numel() )
	{
		Tensor<double> sqS = S.power(2.0) ; // sqS = S^2
		double sumAll = sqS.sum() ;

		Tensor<double> sqScut = sqS.subTensor(Dcut, sqS.numel() - 1) ;

		double sumCut = sqScut.sum() ;
		// {matlab} truncationError = sum(S(D+1 : length(S)).^2) / sum(S.^2) ;
		truncationError = sumCut / sumAll ;


		S = S.subTensor(0, Dcut - 1) ;

		U = U.subTensor(0, U.dimension(0) - 1, 0, Dcut - 1) ;
		V = V.subTensor(0, V.dimension(0) - 1, 0, Dcut - 1) ;
	}
	else
	{
		truncationError = 0 ;
	}
}

void ImaginaryTimeEvolutionHoneycomb::truncateSVD(Parameter &parameter,
		Tensor<double>& U, Tensor<double>& S, Tensor<double>& V)
{
	int Dcut = getDcut(parameter, S) ;

	if ( Dcut < S.numel() )
	{
		S = S.subTensor(0, Dcut - 1) ;
		U = U.subTensor(0, U.dimension(0) - 1, 0, Dcut - 1) ;
		V = V.subTensor(0, V.dimension(0) - 1, 0, Dcut - 1) ;
	}
}

void ImaginaryTimeEvolutionHoneycomb::truncateSVD(const int Dcut,
		Tensor<double>& U, Tensor<double>& S, Tensor<double>& V)
{
	if ( Dcut < S.numel() )
	{
		S = S.subTensor(0, Dcut - 1) ;
		U = U.subTensor(0, U.dimension(0) - 1, 0, Dcut - 1) ;
		V = V.subTensor(0, V.dimension(0) - 1, 0, Dcut - 1) ;
	}
}

int ImaginaryTimeEvolutionHoneycomb::getDcut(Parameter &parameter, Tensor<double>& S)
{
	int Dcut = min(parameter.TNS_bondDim, S.numel()) ;
	double minVal = parameter.ITE_simUpd_minVal ;

	bool flag = false ;
	while ( S(Dcut - 1) < minVal )
	{
		Dcut = Dcut - 1 ;
		flag = true ;
	}

	if (flag)
	{
		cout << "singular value is too small" << endl ;
//		S.display() ;
	}

	return Dcut ;
}

// PR(z',z) = sum{r}_[R1(r,z')*V(r,z)/S(z)]
Tensor<double> ImaginaryTimeEvolutionHoneycomb::createProjector(
		Tensor<double> R1, Tensor<double> V, Tensor<double> S)
{
	// V(r,z) = V(r,z)/S(z)
	V = spitVector(V, 1, S) ;

	// PR(z',z) = sum{r}_[R1(r,z')*V(r,z)]
	Tensor<double> PR = contractTensors(R1, 0, V, 0) ;

	PR = PR / PR.norm('1') ;

	return PR ;
}

//=======================================================================================
void ImaginaryTimeEvolutionHoneycomb::applyWaveProjection_full(
		Parameter parameter, WaveFunction &wave)
{
	int bondDim = parameter.TNS_bondDim ;
	cout << "site dimension = " << parameter.siteDim << endl ;
	cout << "wave function bond dimension = " << bondDim << endl ;

	double tau = parameter.ITE_tauInitial_full ;
	double tauFinal = parameter.ITE_tauFinal_full ;
	int tauReduceStep = parameter.ITE_tauReduceStep_full ;
	int tauFinalStep = parameter.ITE_tauFinalStep_full ;
	double tauReduceRatio = pow( 0.1,  1.0 / double(tauReduceStep - 1)) ;
	cout << "tau reduce ratio: " << tauReduceRatio << endl ;

	int step_perCalEng = parameter.ITE_step_perCalEng ;

	// count the number of total steps
	int j = wave.fullStep ;
	// create polarized hamiltonian
	// 3 terms
	TensorArray<double> hamiltonian = createHamiltonian(parameter) ;
	//---------------------------------------------------------
	string dirName = "result/KitaevHeisenberg/imaginaryTimeEvolution" ;
	dirName = dirName + "/D" + num2str(bondDim) ;
	int nsites = 8 * pow((double)3, parameter.RGsteps) ;
	dirName = dirName + "/sites_" + num2str(nsites) ;
	makeDir(dirName) ;

	string fileName = "/ITEchi" + num2str(parameter.ITE_RG_bondDim) + ".txt" ;
	fileName = dirName + fileName ;

	ofstream outfile(&(fileName[0]), ios::out) ;
	if (!outfile)
	{
		cerr<<"applyWaveProjection_full: open error!" << std::endl ;
		exit(0) ;
	}
	outfile << setw(12) << "step" << setw(11) << "|" ;
	outfile << setw(12) << "tau" << setw(11) << "|" ;
	outfile << setw(12) << "energy" << setw(11) << "|" ;
	outfile << setw(12) << "EVCchi = " << parameter.EVC_RG_bondDim << endl ;
	int wid = 21 ;
	outfile.precision(15) ;
	//---------------------------------------------------------
	// compute the ground state energy in the consecutive steps.
	double energy0 = 10.0 ;
	double energy = 10.0 ;
	double converge = 10.0 ; // measure converge error by energy
	//---------------------------------------------------------
	while (tau >= tauFinal)
	{
		//-------------------------------------------------------------------
		j ++ ;
		cout << "==============================================================" << endl ;
		cout << "full update step No. " << j << ", tau = " << tau << endl ;
		double t_start = dsecnd() ;
		if ( parameter.ITE_fullInitial == 'S' )
		{
			applyFirstTrotter_full(parameter, hamiltonian, tau, wave, energy) ;
		}
		else if ( parameter.ITE_fullInitial == 'B' )
		{
			applyFirstTrotter_SRG(parameter, hamiltonian, tau, wave, energy) ;
		}
		double t_end = dsecnd() ;
		//*******************************************************************
		cout << "full update time: " << (t_end - t_start) << " s" << endl ;
		//-------------------------------------------------------------------
		if ( (j % step_perCalEng) == 0  )
		{
			cout << "****************************************************************************" << endl ;
			KitaevHeisenberg KH ;
			energy = KH.findEnergy(parameter, wave) ;
			outfile << setw(wid) << j << " |" ;
			outfile << setw(wid) << tau << " |" ;
			outfile << setw(wid) << energy << " |" << endl ;
			if ( energy < (energy0 + 1e-5) )
			{
				wave.fullStep = j ;
				wave.tau = tau ;
				wave.save() ;

				converge = fabs( (energy - energy0) / energy ) ;
				cout << "energy convergence error = " <<  converge << endl ;
			}
			energy0 = energy ;
			cout << "****************************************************************************" << endl ;
		}

		tau = tau * tauReduceRatio ;
		if ((tau < tauFinal) && (tauFinalStep > 1))
		{
			tau = tauFinal ;
			tauFinalStep -- ;
		}
	}
	outfile.close() ;
	cout << "total full update steps: " << j << endl ;
	cout << "***********************************************************************************" << endl ;
}

void ImaginaryTimeEvolutionHoneycomb::applyFirstTrotter_full(Parameter parameter,
		TensorArray<double> hamiltonian, double tau,
		WaveFunction &wave, double &energy)
{
	cout << "ITE_RG_bondDim: " << parameter.ITE_RG_bondDim << endl ;
	cout << "ITE_sweep_maxIt: " << parameter.ITE_sweep_maxIt << endl ;
	Tensor<double> energyBond(3) ;
	//****************************************************************
	for (int i = 0; i < 3; i ++)
	{
		if ( gaugeFix == true )
		{
			fullUpdate_GauFix(parameter, hamiltonian(i), tau, wave, energyBond(i)) ;
		}
		else
		{
			fullUpdate(parameter, hamiltonian(i), tau, wave, energyBond(i),
					rightProjectors(i), leftProjectors(i)) ;
		}
		wave.rotate() ;
	}
	energy = energyBond.mean() * 1.5 ;
}

void ImaginaryTimeEvolutionHoneycomb::applyFirstTrotter_SRG(Parameter parameter,
		TensorArray<double> hamiltonian, double tau,
		WaveFunction &wave, double &energy)
{
	cout << "ITE_RG_bondDim: " << parameter.ITE_RG_bondDim << endl ;
	Tensor<double> energyBond(3) ;
	//****************************************************************
	for (int i = 0; i < 3; i ++)
	{
		SRGfullUpdate(parameter, hamiltonian(i), tau, wave, energyBond(i),
				rightProjectors(i), leftProjectors(i)) ;
		wave.rotate() ;
	}

	energy = energyBond.mean() * 1.5 ;
}

//----------------------------------------------------------------------------------------

// H01 z bond
// use simple update projectors PR and PL as initial value
// output: wave, energyBond, PR, PL
void ImaginaryTimeEvolutionHoneycomb::fullUpdate(Parameter parameter,
		Tensor<double> hamiltonian, double tau,
		WaveFunction &wave, double &energyBond, Tensor<double> &PR, Tensor<double> &PL)
{
	TensorArray<double> PO = createProjectionOperator(parameter, hamiltonian, tau) ;
	//--------------------------------------------------------------------------
	Tensor<double> T0, T1 ;
	// T0(z',x0,y0,m0) T1(z',x1,y1,m1)
	computeProjection(PO, wave.A(0), wave.A(1), T0, T1) ;

	//--------------------------------------------------------------------------
	// step A: absorb lambdas
	// M0((x0,y0,m0),z') = T0(z',x0,y0,m0)*sqrt(L(0,1)(x0) * L(0,2)(y0))
	Tensor<double> M0 = computeT_sqrtLxLy(T0, wave.Lambda(0, 1), wave.Lambda(0, 2)) ;
	// M1((x1,y1,m1),z') = T1(z',x1,y1,m1)*sqrt(L(1,1)(x1) * L(1,2)(y1))
	Tensor<double> M1 = computeT_sqrtLxLy(T1, wave.Lambda(1, 1), wave.Lambda(1, 2)) ;

	//-------------------------------------------------------------------------

	// step B: QR factorization
	// M0((x0,y0,m0),z') = sum{l}_[Q0((x0,y0,m0),l)*R0(l,z')]
	Tensor<double> R0 ;
	qr(M0, R0) ;
	R0 = R0 / R0.norm('1') ;

	// M1((x1,y1,m1),z') = sum{r}_[Q1((x1,y1,m1),r)*R1(r,z')]
	Tensor<double> R1 ;
	qr(M1, R1) ;
	R1 = R1 / R1.norm('1') ;

	//-------------------------------------------------------------------------
	// step C: SVD and truncation
	// M(l,r) = sum{z'}_[R0(l,z')*R1(r,z')]
	Tensor<double> M = contractTensors(R0, 1, R1, 1) ;

	// M(l,r) = sum{z}_[U(l,z)*S(z)*V(r,z)]
	Tensor<double> U, S, V;
	svd_qr(M, U, S, V) ;
//	S.display() ;

	S = S / S(0) ;

	//
	truncateSVD(parameter, U, S, V) ;

	Tensor<double> sqrtS = S.sqrt() ;

	//-------------------------------------------------------------------------
	// step D: Create Projector
	// PR(z',z) = sum{r}_[R1(r,z')*V(r,z)/sqrtS(z)]
	PR = createProjector(R1, V, sqrtS) ;

	// PL(z',z) = sum{l}_[R0(l,z')*U(l,z)/sqrtS(z)]
	PL = createProjector(R0, U, sqrtS) ;

	wave.Lambda(0, 0) = S ;
	wave.Lambda(1, 0) = S ;
	//*************************************************************************
	// output: energyBond, PR, PL
	variation(parameter, wave, hamiltonian, energyBond, T0, T1, PR, PL) ;
	//-------------------------------------------------------------------------
	// step E. Form new system sites: wave.A(0), wave.A(1),
	// A0(z,x0,y0,m0) = sum{z'}_[PR(z',z) * T0(z',x0,y0,m0)]
	wave.A(0) = contractTensors(PR, 0, T0, 0) ;

	// A1(z,x1,y1,m1) = sum{z'}_[PL(z',z) * T1(z',x1,y1,m1)]
	wave.A(1) = contractTensors(PL, 0, T1, 0) ;
}

//----------------------------
// output: energyBond, PR, PL
void ImaginaryTimeEvolutionHoneycomb::variation(Parameter parameter, WaveFunction &wave,
		Tensor<double> H, double &energyBond, Tensor<double> &Ap0, Tensor<double> &Ap1,
		Tensor<double> &PR, Tensor<double> &PL)
{
	parameter.RG_method = parameter.ITE_RG_method ;
	parameter.SRG_trunc = parameter.ITE_SRG_trunc ;
	parameter.SRG_norm  = parameter.ITE_SRG_norm  ;

	parameter.RG_bondDim = parameter.ITE_RG_bondDim ;
	parameter.RG_minVal = parameter.ITE_RG_minVal ;
	parameter.sweep_maxIt = parameter.ITE_sweep_maxIt ;
	//--------------------------------------------------------------------------------------------
	// compute environment tensors and bond energy
	TensorArray<double> T(2) ;
	for (int i = 0; i < 2; i ++)
	{
		// T(0)((z0,z0'),(x0,x0'),(y0,y0')) = sum{m0}_[A(0)(z0,x0,y0,m0)*A(0)(z0',x0',y0',m0)]
		T(i) = tracePhysInd(wave.A(i)) ;
	}
	EnvironmentHoneycomb env(parameter, T) ;

	// environment of site 0 and 1 along z bond
	Tensor<double> Menv = env.compute_Menv() ; // Menv(x0,y0,x1,y1)

	energyBond = env.computeBond(parameter, wave.A, T, Menv, H) ;
//	cout << "energyBond z: " << energyBond << endl ;
	//--------------------------------------------------------------------------------------------
	// compute Me(zr,zr',zl,zl')
	Tensor<double> Me = contractEnv(Menv, Ap0, Ap1) ;
	//--------------------------------------------------------------------------------------------
	// optimize PR and PL
	optimizeProjectors(parameter, Me, PR, PL) ;
}
//*****************************************************************************************

void ImaginaryTimeEvolutionHoneycomb::fullUpdate_GauFix(Parameter parameter,
		Tensor<double> hamiltonian, double tau,
		WaveFunction &wave, double &energyBond)
{
	Tensor<double> X, Y, aR, bL;
	// A0(z,x0,y0,n0) = sum{r}_[X(x0,y0,r)*aR(r,z,n0)]
	createReduTen(wave.A(0), X, aR) ; // qr factorization
	// A1(z,x1,y1,n1) = sum{l}_[Y(x1,y1,l)*bL(l,z,n1)]
	createReduTen(wave.A(1), Y, bL) ;
	//==========================================================================
	Tensor<double> PO ;
	// PO(n0,n1,m0,m1)
	createProjectionOperator(parameter, hamiltonian, tau, PO) ;
	// after time evolution, before truncation
	Tensor<double> aR0, bL0 ;
	// aR, bL are initial truncated reduced tensors
	initializeRL(aR, bL, aR0, bL0, PO, parameter.TNS_bondDim) ;
	//*********************************************
	// output: energyBond, aR(r,z,n0), bL(l,z,n1)
	variation_GauFix(parameter, wave, hamiltonian, energyBond, X, Y,
			aR, bL, aR0, bL0) ;
	//---------------------------------------------
	Index x0, y0, r, z, n0 ;
	X.putIndex(x0, y0, r) ;
	aR.putIndex(r, z, n0) ;
	// A0(x0,y0,z,n0) = sum{r}_[X(x0,y0,r)*aR(r,z,n0)]
	wave.A(0) = contractTensors(X, aR) ;
	// A0(z,x0,y0,n0)
	wave.A(0) = wave.A(0).permute(z, x0, y0, n0) ;

	Index x1, y1, l, n1 ;
	Y.putIndex(x1, y1, l) ;
	bL.putIndex(l, z, n1) ;
	// A1(x1,y1,z,n1) = sum{l}_[Y(x1,y1,l)*bL(l,z,n1)]
	wave.A(1) = contractTensors(Y, bL) ;
	// A1(z,x1,y1,n1)
	wave.A(1) = wave.A(1).permute(z, x1, y1, n1) ;
}

// A(z,x0,y0,n0) = sum{r}_[X(x0,y0,r)*R(r,z,n0)]
void ImaginaryTimeEvolutionHoneycomb::createReduTen(Tensor<double> A, Tensor<double>& X, Tensor<double>& R)
{
	Index z, x0, y0, n0;
	A.putIndex(z, x0, y0, n0) ;
	A = A.permute(x0,y0,z,n0) ;

	A = A.reshape(Index(x0,y0), Index(z,n0)) ;
	// A((x0,y0),(z,n0)) = sum{r}_[X((x0,y0),r)*R(r,(z,n0))]
	qr(A, X, R) ;

	Index r(X.dimension(1)) ;

	X = X.reshape(x0, y0, r) ;
	R = R.reshape(r, z, n0) ;
}

// PO(n0,n1,m0,m1)
void ImaginaryTimeEvolutionHoneycomb::createProjectionOperator(
		Parameter parameter, Tensor<double> hamiltonian, double tau, Tensor<double>& PO)
{
	int M = parameter.siteDim ;
	int MM = M * M ;

	Tensor<double> V(MM, MM) ;
	Tensor<double> D(MM) ;
	// hamiltonian((n0, n1), (m0, m1)) = V * D * V'
	symEig(hamiltonian, V, D) ;

	D = (- tau) * D ;
	// expD(l)
	Tensor<double> expD = D.exp() ;

	Tensor<double> VD = V ;
	// VD((n0,n1),l) = V((n0,n1),l) * expD(l)
	VD = absorbVector(V, 1, expD) ;

	// PO((n0,n1),(m0,m1))
	PO = contractTensors(VD, 1, V, 1) ;

	PO.setSmalltoZero(1e-13) ;
	//------------------------------------------------------------
	// PO(n0,n1,m0,m1)
	PO = PO.reshape(M, M, M, M) ;
}
// input: aR(r,z,n0), bL(l,z,n1)
// output: aR,bL,aR0,bL0,PO(n0,n1,m0,m1)
void ImaginaryTimeEvolutionHoneycomb::initializeRL(Tensor<double>& aR, Tensor<double>& bL,
		Tensor<double>& aR0, Tensor<double>& bL0, Tensor<double>& PO,
		const int Dcut)
{
	Index r, z, n0, l, n1 ;
	Index m0, m1 ;
	aR.putIndex(r, z, n0) ;
	bL.putIndex(l, z, n1) ;
	PO.putIndex(n0, n1, m0, m1) ;
	// theta(r,n0,l,n1) = sum{z}_[aR(r,z,n0)*bL(l,z,n1)]
	Tensor<double> theta = contractTensors(aR, bL) ;
	// theta(r,l,m0,m1) = sum{n0,n1}_[theta(r,n0,l,n1)*PO(n0,n1,m0,m1)]
	theta = contractTensors(theta, PO) ;
	// theta(r,l,m0,m1) -> theta(r,m0, l,m1)
	theta = theta.permute(r, m0, l, m1) ;

	int dr = aR.dimension(0) ; // aR(r,z,n0)
	int dl = bL.dimension(0) ; // bL(l,z,n1)
	int dm = PO.dimension(0) ; // PO(n0,n1,m0,m1)
	// theta((r,m0),(l,m1))
	theta = theta.reshape(dr * dm, dl * dm) ;
	// theta((r,m0),(l,m1)) = sum{z}_[U((r,m0),z)*S(z)*V((l,m1),z)]
	Tensor<double> U, S, V ;
	svd_qr(theta, U, S, V) ;
	S = S / S(0) ;

	Tensor<double> sqrtS = S.sqrt() ;
	z = Index(S.numel()) ;

	int dz = S.numel() ;

	// aR0((r,m0),z) = U((r,m0),z) * sqrtS(z)
	aR0 = absorbVector(U, 1, sqrtS) ;
	aR0 = aR0.reshape(dr, dm, dz) ;
	aR0.putIndex(r, m0, z) ;
	aR0 = aR0.permute(r, z, m0) ;
	// bL0((l,m1),z) = V((l,m1),z) * sqrtS(z)
	bL0 = absorbVector(V, 1, sqrtS) ;
	bL0 = bL0.reshape(dl, dm, dz) ;
	bL0.putIndex(l, m1, z) ;
	bL0 = bL0.permute(l, z, m1) ;
	//----------------------------------------
	truncateSVD(Dcut, U, S, V) ;
	sqrtS = S.sqrt() ;
	z = Index(S.numel()) ;
	dz = S.numel() ;
	// aR((r,m0),z) = U((r,m0),z) * sqrtS(z)
	aR = absorbVector(U, 1, sqrtS) ;
	aR = aR.reshape(dr, dm, dz) ;
	aR.putIndex(r, m0, z) ;
	aR = aR.permute(r, z, m0) ;
	// bL((l,m1),z) = V((l,m1),z) * sqrtS(z)
	bL = absorbVector(V, 1, sqrtS) ;
	bL = bL.reshape(dl, dm, dz) ;
	bL.putIndex(l, m1, z) ;
	bL = bL.permute(l, z, m1) ;
}

void ImaginaryTimeEvolutionHoneycomb::variation_GauFix(
		Parameter parameter, WaveFunction &wave, Tensor<double> H,
		double &energyBond, Tensor<double> &X, Tensor<double> &Y,
		Tensor<double> &aR, Tensor<double> &bL,
		Tensor<double> &aR0, Tensor<double> &bL0)
{
	parameter.RG_method = parameter.ITE_RG_method ;
	parameter.SRG_trunc = parameter.ITE_SRG_trunc ;
	parameter.SRG_norm  = parameter.ITE_SRG_norm  ;

	parameter.RG_bondDim = parameter.ITE_RG_bondDim ;
	parameter.RG_minVal = parameter.ITE_RG_minVal ;
	parameter.sweep_maxIt = parameter.ITE_sweep_maxIt ;
	//-----------------------------------------------------
	// compute environment tensors and bond energy
	TensorArray<double> T(2) ;
	for (int i = 0; i < 2; i ++)
	{
		// T(0)((z0,z0'),(x0,x0'),(y0,y0')) =
		// sum{m0}_[A(0)(z0,x0,y0,m0)*A(0)(z0',x0',y0',m0)]
		T(i) = tracePhysInd(wave.A(i)) ;
	}
	EnvironmentHoneycomb env(parameter, T) ;

	// environment of site 0 and 1 along z bond
	Tensor<double> Menv = env.compute_Menv() ; // Menv(x0,y0,x1,y1)

	energyBond = env.computeBond(parameter, wave.A, T, Menv, H) ;
//	cout << "energyBond z: " << energyBond << endl ;
	//-----------------------------------------------------------
	// NLR(r,l,r',l')
	Tensor<double> NLR = computeNormTensor(Menv, X, Y) ;

	//
	if ( preCond )
	{
		gauFix(NLR, X, Y, aR, bL, aR0, bL0) ;
	}
	else
	{
		positiveApprox(NLR) ;
	}

	//
	optimizeRL(parameter, NLR, aR, bL, aR0, bL0) ;
}

// Menv((x0,x0'),(y0,y0'),(x1,x1'),(y1,y1')), X(x0,y0,r), Y(x1,y1,l)
Tensor<double> ImaginaryTimeEvolutionHoneycomb::computeNormTensor(Tensor<double> &Menv,
		Tensor<double> &X, Tensor<double> &Y)
{
	int dx0 = X.dimension(0) ;
	int dy0 = X.dimension(1) ;
	int dx1 = Y.dimension(0) ;
	int dy1 = Y.dimension(1) ;

	Menv = Menv.reshape(dx0, dx0, dy0, dy0, dx1, dx1, dy1, dy1) ;

	Index x0, x0p, y0, y0p, x1, x1p, y1, y1p ;
	Index r, l, rp, lp ;
	Menv.putIndex(x0, x0p, y0, y0p, x1, x1p, y1, y1p) ;
	X.putIndex(x0, y0, r) ;
	Y.putIndex(x1, y1, l) ;
	// NLR(x0p, y0p, x1, x1p, y1, y1p,r) =
	// sum{x0,y0}_[Menv(x0,x0p,y0, y0p, x1, x1p, y1, y1p)*X(x0,y0,r)]
	Tensor<double> NLR = contractTensors(Menv, X) ;
	// NLR(x0p,y0p,x1p,y1p,r,l) = sum{x1,y1}_[NLR(x0p,y0p,x1,x1p,y1,y1p,r)*Y(x1,y1,l)]
	NLR = contractTensors(NLR, Y) ;

	X.putIndex(x0p, y0p, rp) ;
	Y.putIndex(x1p, y1p, lp) ;
	// NLR(x1p,y1p,r,l,rp) = sum{x0p,y0p}_[NLR(x0p,y0p,x1p,y1p,r,l)*X(x0p,y0p,rp)]
	NLR = contractTensors(NLR, X) ;
	// NLR(r,l,rp,lp) = sum{x1p,y1p}_[NLR(x1p,y1p,r,l,rp)*Y(x1p,y1p,lp)]
	NLR = contractTensors(NLR, Y) ;

	NLR = NLR / NLR.maxAbs() ;
	return NLR ;
}

void ImaginaryTimeEvolutionHoneycomb::gauFix(
		Tensor<double>& NLR, Tensor<double>& X, Tensor<double>& Y,
    Tensor<double> &aR, Tensor<double> &bL,
	  Tensor<double> &aR0, Tensor<double> &bL0)
{
	int dr = NLR.dimension(0) ;
	int dl = NLR.dimension(1) ;
	NLR = NLR.reshape(dr * dl, dr * dl) ;
	NLR.symmetrize() ;
	//-----------------------------------
	Tensor<double> W, Sigma ;
	symEig(NLR, W, Sigma) ;
	Sigma = Sigma / Sigma.maxAbs() ;
//	Sigma.display() ;
	truncateSmallEig(W, Sigma) ;
//	Sigma.display() ;
	int dk = Sigma.numel() ;

	Tensor<double> sqrtSigma = Sigma.sqrt() ;
	// Z((r,l),k) = W((r,l),k)*sqrtSigma(k)
	Tensor<double> Z = absorbVector(W, 1, sqrtSigma) ;
	// Z((r,l),k) -> Z(r,l,k)
	Z = Z.reshape(dr, dl, dk) ;
	Index r, l, k ;
	Z.putIndex(r, l, k) ;
	//-----------------------------------
	Tensor<double> ZL = Z.permute(r, k, l) ;
	ZL = ZL.reshape(dr * dk, dl) ; // ZL((r,k),l)
	Tensor<double> QL, R ;
	// ZL((r,k),l) = sum{p}_[QL((r,k),p)*R(p,l)]
	qr(ZL, QL, R) ;
	Tensor<double> Rinv = pinv(R, 0) ; // Rinv(l,p)

	Tensor<double> ZR = Z.permute(l, k, r) ;
	ZR = ZR.reshape(dl * dk, dr) ; // ZR((l,k),r)
	Tensor<double> QR, L ;
	// ZR((l,k),r) = sum{q}_[QR((l,k),q)*L(q,r)]
	qr(ZR, QR, L) ;
	Tensor<double> Linv = pinv(L, 0) ; // Linv(r,q)
	//---------------------------------------------
	Index p, q ;
	Rinv.putIndex(l, p) ;
	Linv.putIndex(r, q) ;
	// Z(r,k,p) = sum{l}_[Z(r,l,k)*Rinv(l,p)]
	Z = contractTensors(Z, Rinv) ;
	// Z(q,k,p) = sum{r}_[Linv(r,q)*Z(r,k,p)]
	Z = contractTensors(Linv, Z) ;
	//*********************
	// NLR(q,p,q',p') = sum{k}_[Z(q,k,p)*Z(q',k,p')]
	NLR = contractTensors(Z, 1, Z, 1) ;
	//*********************
	Index x0, y0;
	// X(x0,y0,r)
	X.putIndex(x0, y0, r) ;
	// X(x0,y0,q) = sum{r}_[X(x0,y0,r)*Linv(r,q)]
	X = contractTensors(X, Linv) ;

	Index x1, y1 ;
	// Y(x1,y1,l)
	Y.putIndex(x1,y1,l) ;
	// Y(x1,y1,p) = sum{l}_[Y(x1,y1,l)*Rinv(l,p)]
	Y = contractTensors(Y, Rinv) ;
	//*********************
	Index z, n0 ;
	aR.putIndex(r, z, n0) ;
	aR0.putIndex(r, z, n0) ;
	L.putIndex(q, r) ;
	// aR(q,z,n0) = sum{r}_[L(q,r)*aR(r,z,n0)]
	aR = contractTensors(L, aR) ;
	aR0 = contractTensors(L, aR0) ;

	Index n1 ;
	bL.putIndex(l, z, n1) ;
	bL0.putIndex(l, z, n1) ;
	R.putIndex(p, l) ;
	// bL(p,z,n1) = sum{l}_[R(p,l) * bL(l,z,n1)]
	bL = contractTensors(R, bL) ;
	bL0 = contractTensors(R, bL0) ;
}

void ImaginaryTimeEvolutionHoneycomb::positiveApprox(Tensor<double>& NLR)
{
	int dr = NLR.dimension(0) ;
	int dl = NLR.dimension(1) ;
	NLR = NLR.reshape(dr * dl, dr * dl) ;
	NLR.symmetrize() ;
	//-----------------------------------
	Tensor<double> W, Sigma ;
	symEig(NLR, W, Sigma) ;
	Sigma = Sigma / Sigma.maxAbs() ;
//	Sigma.display() ;
	truncateSmallEig(W, Sigma) ;
//	Sigma.display() ;

	int dk = Sigma.numel() ;

	Tensor<double> sqrtSigma = Sigma.sqrt() ;
	// Z((r,l),k) = W((r,l),k)*sqrtSigma(k)
	Tensor<double> Z = absorbVector(W, 1, sqrtSigma) ;
	// Z((r,l),k) -> Z(r,l,k)
	Z = Z.reshape(dr, dl, dk) ;
	Index r, l, k ;
	Z.putIndex(r, l, k) ;
	//**********************************************
	// NLR(r,l,r',l') = sum{k}_[Z(r,l,k)*Z(r',l',k)]
	NLR = contractTensors(Z, 2, Z, 2) ;
}

// W((r,l),k) Sigma(k)
void ImaginaryTimeEvolutionHoneycomb::truncateSmallEig(Tensor<double>& W, Tensor<double>& Sigma)
{
	int indStart = 0 ;
	while ( Sigma(indStart) < minEig )
	{
		indStart++ ;
	}

	int drl = W.dimension(0) ;
	int dk = Sigma.numel() ;

	Sigma = Sigma.subTensor(indStart, dk - 1) ;
	W = W.subTensor(0, drl - 1, indStart, dk - 1) ;
}

// NLR(r,l,rp,lp)
// aR(r,z,n0), aR0(r,z,n0), bL(l,z,n1), bL0(l,z,n1)
void ImaginaryTimeEvolutionHoneycomb::optimizeRL(Parameter parameter, Tensor<double>& NLR,
		Tensor<double>& aR, Tensor<double>& bL, Tensor<double>& aR0, Tensor<double>& bL0)
{
	double tol = parameter.ITE_tol_LinEq ;
	int maxStep = parameter.ITE_maxLinEqStep ;
	//-------------------------------------------------------------
	Tensor<double> costFunc(2) ;
	Tensor<double> costFunc0(2) ;
	costFunc0 = 100 ;
	Tensor<double> ConvErr(2) ;

	int step = 0 ;
	double converge = 10.0 ;

	while ( (converge > tol) && (step < maxStep) )
	{
		step ++ ;
		costFunc(0) = optimize_aR(parameter, NLR, aR, bL, aR0, bL0) ;
		costFunc(1) = optimize_bL(parameter, NLR, aR, bL, aR0, bL0) ;

		ConvErr = costFunc - costFunc0 ;
		for ( int i = 0; i < 2; i ++ )
		{
			ConvErr(i) = fabs(ConvErr(i) / costFunc(i)) ;
		}
		converge = ConvErr.maxAbs() ;
//		cout << "converge: " << converge << endl ;
		costFunc0 = costFunc ;
	}
	cout << "Solving linear equation steps: " << step << endl ;
	if ( converge > tol )
	{
		cout << "variation is not converged, linear equation error: " << converge << endl ;
	}
}

// NLR(r,l,rp,lp)
// aR(r,z,n0),  bL(l,z,n1),
// aR0(rp,z,n0), bL0(lp,z,n1)
double ImaginaryTimeEvolutionHoneycomb::optimize_aR(Parameter parameter, Tensor<double>& NLR,
		Tensor<double>& aR, Tensor<double>& bL, Tensor<double>& aR0, Tensor<double>& bL0)
{
	Index r, l,  rp, lp ;
	Index z, zp, n0, n1 ;
	aR0.putIndex(rp, z, n0) ;
	bL0.putIndex(lp, z, n1) ;
	// N(rp,n0,lp,n1) = sum{z}_[aR0(rp,z,n0)*bL0(lp,z,n1)]
	Tensor<double> N = contractTensors(aR0, bL0) ;
	NLR.putIndex(r, l, rp, lp) ;
	// N(r,l,n0,n1) = sum{rp,lp}_[NLR(r,l,rp,lp)*N(rp,n0,lp,n1)]
	N = contractTensors(NLR, N) ;
	bL.putIndex(l, z, n1) ;
	// N(z,r,n0) = sum{l,n1}_[bL(l,z,n1)*N(r,l,n0,n1)]
	N = contractTensors(bL, N) ;
	// N(r,z,n0)
	N = N.permute(r, z, n0) ;
	//**************************************************************
	// M(l,z,lp,zp) = sum{n1}_[bL(l,z,n1)*bL(lp,zp,n1)]
	Tensor<double> M = contractTensors(bL, 2, bL, 2) ;
	M.putIndex(l,z,lp,zp) ;
	// M(r,rp,z,zp) = sum{l,lp}_[NLR(r,l,rp,lp)*M(l,z,lp,zp)]
	M = contractTensors(NLR, M) ;
	M = M.permute(r, z, rp, zp) ;

	int dr = M.dimension(0) ;
	int dz = M.dimension(1) ;
	// M((r,z),(rp,zp))
	M = M.reshape(dr*dz, dr*dz) ;
	//*************************************************************
	// Minv((rp,zp),(r,z))
	Tensor<double> Minv = pinv(M, parameter.ITE_LinEq_minSing) ;
	// Minv(rp,zp,r,z)
	Minv = Minv.reshape(dr, dz, dr, dz) ;
	Minv.putIndex(rp, zp, r, z) ;
	// aR(rp,zp,n0) = sum{r,z}_[Minv(rp,zp,r,z)*N(r,z,n0)]
	aR = contractTensors(Minv, N) ;
	//*************************************************************
	// cost function
	M = M.reshape(dr, dz, dr, dz) ;
	M.putIndex(r, z, rp, zp) ;
	// RMR(r,z,n0) = sum{rp,zp}_[M(r,z,rp,zp)*aR(rp,zp,n0)]
	Tensor<double> RMR = contractTensors(M, aR) ;
	aR.putIndex(r,z,n0) ;
	// RMR = sum{r,z,n0}_[RMR(r,z,n0)*aR(r,z,n0)]
	RMR = contractTensors(RMR, aR) ;

	// NR = sum{r,z,n0}_[N(r,z,n0)*aR(r,z,n0)]
	Tensor<double> NR = contractTensors(N, aR) ;

	double costFunc = RMR(0) - 2 * NR(0) ;
	return costFunc ;
}

double ImaginaryTimeEvolutionHoneycomb::optimize_bL(Parameter parameter, Tensor<double>& NLR,
		Tensor<double>& aR, Tensor<double>& bL, Tensor<double>& aR0, Tensor<double>& bL0)
{
	Index r, l,  rp, lp ;
	Index z, zp, n0, n1 ;
	aR0.putIndex(rp, z, n0) ;
	bL0.putIndex(lp, z, n1) ;
	// N(rp,n0,lp,n1) = sum{z}_[aR0(rp,z,n0)*bL0(lp,z,n1)]
	Tensor<double> N = contractTensors(aR0, bL0) ;
	NLR.putIndex(r, l, rp, lp) ;
	// N(r,l,n0,n1) = sum{rp,lp}_[NLR(r,l,rp,lp)*N(rp,n0,lp,n1)]
	N = contractTensors(NLR, N) ;
	aR.putIndex(r, z, n0) ;
	// N(z,l,n1) = sum{r,n0}_[aR(r,z,n0)*N(r,l,n0,n1)]
	N = contractTensors(aR, N) ;
	// N(l,z,n1)
	N = N.permute(l, z, n1) ;
	//*************************************************************
	// M(r,z,rp,zp) = sum{n0}_[aR(r,z,n0)*aR(rp,zp,n0)]
	Tensor<double> M = contractTensors(aR, 2, aR, 2) ;
	M.putIndex(r, z, rp, zp) ;
	// M(l,lp,z,zp) = sum{r,rp}_[NLR(r,l,rp,lp)*M(r,z,rp,zp)]
	M = contractTensors(NLR, M) ;
	M = M.permute(l, z, lp, zp) ;

	int dl = M.dimension(0) ;
	int dz = M.dimension(1) ;
	// M((l,z),(lp,zp))
	M = M.reshape(dl * dz, dl * dz) ;
	//*************************************************************
	// Minv((lp,zp),(l,z))
	Tensor<double> Minv = pinv(M, parameter.ITE_LinEq_minSing) ;
	// Minv(lp,zp,l,z)
	Minv = Minv.reshape(dl, dz, dl, dz) ;
	Minv.putIndex(lp, zp, l, z) ;
	// bL(lp,zp,n1) = sum{l,z}_[Minv(lp,zp,l,z)*N(l,z,n1)]
	bL = contractTensors(Minv, N) ;
	//*************************************************************
	// cost function
	M = M.reshape(dl, dz, dl, dz) ;
	M.putIndex(l, z, lp, zp) ;
	// RMR(l,z,n1) = sum{lp,zp}_[M(l,z,lp,zp)*bL(lp,zp,n1)]
	Tensor<double> RMR = contractTensors(M, bL) ;
	bL.putIndex(l,z,n1) ;
	// RMR = sum{l,z,n1}_[RMR(l,z,n1)*bL(l,z,n1)]
	RMR = contractTensors(RMR, bL) ;
	// NR = sum{l,z,n1}_[N(l,z,n1)*bL(l,z,n1)]
	Tensor<double> NR = contractTensors(N, bL) ;

	double costFunc = RMR(0) - 2 * NR(0) ;
	return costFunc ;
}

//*****************************************************************************************

void ImaginaryTimeEvolutionHoneycomb::SRGfullUpdate(Parameter parameter, Tensor<double> hamiltonian, double tau,
		WaveFunction &wave, double &energyBond, Tensor<double> &PR, Tensor<double> &PL)
{
	TensorArray<double> PO = createProjectionOperator(parameter, hamiltonian, tau) ;
	//--------------------------------------------------------------------------
	Tensor<double> Ap1, Ap2 ;
	// Ap1(z',x1,y1,m1) Ap2(z',x2,y2,m2)
	computeProjection(PO, wave.A(0), wave.A(1), Ap1, Ap2) ;
	//-----------------------------------------------------------
	parameter.RG_bondDim = parameter.ITE_RG_bondDim ;
	//*************************************************************************************
	// compute environment tensors and bond energy
	TensorArray<double> T(2) ;
	for (int i = 0; i < 2; i ++)
	{
		// T(1)((z1,z1'),(x1,x1'),(y1,y1')) = sum{m1}_[A(1)(z1,x1,y1,m1)*A(1)(z1',x1',y1',m1)]
		T(i) = tracePhysInd(wave.A(i)) ;
	}
	EnvironmentHoneycomb env(parameter, T) ;
	// environment of site 0 and 1 along z bond
	Tensor<double> Menv = env.compute_Menv() ; // Menv(x0,y0,x1,y1)

	energyBond = env.computeBond(parameter, wave.A, T, Menv, hamiltonian) ;
	//--------------------------------------------------------------------------------------------
	// compute Me(zr,zr',zl,zl')
	Tensor<double> Me = contractEnv(Menv, Ap1, Ap2) ;
	//----------------------------------------------------------------
	// N(zr,zl) = sum{z'}_[Me(zr,z',zl,z')]
	Tensor<double> N = computeN(Me) ;

	// eig with truncation
	decomposeBonDenMat(parameter, N, PR, PL) ;
	//--------------------------------------------------------------------------------------------
	// optimize PR and PL
	optimizeProjectors(parameter, Me, PR, PL) ;
	//-------------------------------------------------------------------------
	// step E. Form new system sites: wave.A(1), wave.A(2),
	// A1(z,x1,y1,m1) = sum{z'}_[PR(z',z) * Ap1(z',x1,y1,m1)]
	wave.A(0) = contractTensors(PR, 1, Ap1, 1) ;

	// A2(z,x2,y2,m2) = sum{z'}_[PL(z',z) * Ap2(z',x2,y2,m2)]
	wave.A(1) = contractTensors(PL, 1, Ap2, 1) ;

}
//********************************************************

// T((z,z'),(x,x'),(y,y')) = sum{m}_[A(z,x,y,m)*A(z',x',y',m)]
Tensor<double> ImaginaryTimeEvolutionHoneycomb::tracePhysInd(Tensor<double> &A)
{
	int dz = A.dimension(0) ;
	int dx = A.dimension(1) ;
	int dy = A.dimension(2) ;
	// T(z,x,y,z',x',y') = sum{m}_[A(z,x,y,m)*A(z',x',y',m)]
	Tensor<double> T = contractTensors(A, 3, A, 3) ;

	// T(z,x,y,z',x',y') -> T(z,z',x,x',y,y')
	T = T.permute(0, 3, 1, 4, 2, 5) ;

	// T(z,z',x,x',y,y') -> T((z,z'),(x,x'),(y,y'))
	T = T.reshape(dz * dz, dx * dx, dy * dy) ;

	return T ;
}

//********************************************************

// output: Me(zr,zr',zl,zl')
// Menv(x0,y0,x1,y1) Ap0(zr,x0,y0,m0) Ap1(zl,x1,y1,m1)
Tensor<double> ImaginaryTimeEvolutionHoneycomb::contractEnv(
		Tensor<double> &Menv, Tensor<double> &Ap0, Tensor<double> &Ap1)
{
	int dr = Ap0.dimension(0) ;
	int dl = Ap1.dimension(0) ;

	Tensor<double> Tp0, Tp1 ;
	// Tp0((zra,zrb),(x0,x0'),(y0,y0')) = sum{m0}_[Ap0(zra,x0,y0,m0)*Ap0(zrb,x0',y0',m0)]
	Tp0 = tracePhysInd(Ap0) ;
	// Tp1((zla,zlb),(x1,x1'),(y1,y1')) = sum{m1}_[Ap1(zla,x1,y1,m1)*Ap1(zlb,x1',y1',m1)]
	Tp1 = tracePhysInd(Ap1) ;

	// change notation: Tp0((zra,zrb),(x0,x0'),(y0,y0')) -> Tp0(zrab,x0,y0)
	// change notation: Tp1((zla,zlb),(x1,x1'),(y1,y1')) -> Tp1(zlab,x1,y1)
	Index zrab, zlab, x0, y0, x1, y1 ;
	Menv.putIndex(x0, y0, x1, y1) ;
	Tp0.putIndex(zrab, x0, y0) ;
	Tp1.putIndex(zlab, x1, y1) ;

	// Me(zrab,x1,y1) = Tp0(zrab,x0,y0) * Mz_01(x0,y0,x1,y1)
	Tensor<double> Me = contractTensors(Tp0, Menv) ;
	// Me(zrab,zlab) = Me(zrab,x1,y1) * Tp1(zlab,x1,y1)
	Me = contractTensors(Me, Tp1) ;

	// Me(zrab,zlab) -> Me(zra,zrb,zla,zlb)
	Me = Me.reshape(dr, dr, dl, dl) ;
	Me = Me / Me.maxAbs() ;

	return Me ;
}

//------------------------------------------------------------------
// Me(zr,zr',zl,zl')
void ImaginaryTimeEvolutionHoneycomb::optimizeProjectors(
		Parameter parameter, Tensor<double> &Me,
		Tensor<double> &PR, Tensor<double> &PL)
{
	double tol = parameter.ITE_tol_LinEq ;
	int maxStep = parameter.ITE_maxLinEqStep ;
	//----------------------------------------------------------------------------------
	Tensor<double> PR0 = PR ;
	Tensor<double> PL0 = PL ;
	Tensor<double> Perr(2) ;

	int step = 0 ;
	double converge = 10.0 ;

	while ( (converge > tol) && (step < maxStep) )
	{
		step ++ ;

		PR = optimizePR(parameter, Me, PL) ;
//		mixProjectors(parameter.ITE_newProjRatio, PR, PR0) ;

		PL = optimizePL(parameter, Me, PR) ;
//		mixProjectors(parameter.ITE_newProjRatio, PL, PL0) ;

		Perr(0) = getDiffAsVector(PR, PR0) ;
		Perr(1) = getDiffAsVector(PL, PL0) ;

		converge = Perr.max() ;

		PR0 = PR ;
		PL0 = PL ;
		//******************************************************************************
//		cout << "Solving linear equation step No. " << step << endl ;
//		cout << "Projectors convergence error = " << converge << endl ;
		//******************************************************************************
	}

	cout << "Solving linear equation steps: " << step << endl ;
	if ( converge > tol )
	{
		cout << "variation is not converged, linear equation error: " << converge << endl ;
	}
}

// Me(xr,xr',xl,xl')
// PR(xr,x2) PL(xl,x2)
Tensor<double> ImaginaryTimeEvolutionHoneycomb::optimizePR(
		Parameter parameter, Tensor<double> &Me, Tensor<double> &PL)
{
	int drl = PL.dimension(0) ;
	int d2 = PL.dimension(1) ;

	// N(xr,xl) = sum{x'}_[Me(xr,x',xl,x')]
	Tensor<double> N = computeN(Me) ;

	// N(xr,x2) = sum{xl}_[N(xr,xl)*PL(xl,x2)]
	N = contractTensors(N, 1, PL, 0) ;

	N = N.reshape(drl * d2, 1) ;
	//**************************************************************
	// M(xr,xr',xl',x2) = sum{xl}_[Me(xr,xr',xl,xl')*PL(xl,x2)]
	Tensor<double> M = contractTensors(Me, 2, PL, 0) ;

	// M(xr,xr',x2,x2') = sum{xl'}_[M(xr,xr',xl',x2)*PL(xl',x2')]
	M = contractTensors(M, 2, PL, 0) ;

	// M(xr,xr',x2,x2') -> M(xr,x2,xr',x2')
	M = M.permute(0, 2, 1, 3) ;

	// M(xr,x2,xr',x2') -> M((xr,x2),(xr',x2'))
	M = M.reshape(drl * d2, drl * d2) ;
	//*************************************************************
	Tensor<double> Minv = pinv(M, parameter.ITE_LinEq_minSing) ;

	Tensor<double> PR = contractTensors(Minv, 1, N, 0) ;

	double coef = PR.norm('2') ;
	PR = PR / coef ;

	PR = PR.reshape(drl, d2) ;

	return PR ;
}

// Me(xr,xr',xl,xl')
// PR(xr,x2) PL(xl,x2)
Tensor<double> ImaginaryTimeEvolutionHoneycomb::optimizePL(
		Parameter parameter, Tensor<double> &Me, Tensor<double> &PR)
{
	// Me(xr,xr',xl,xl') -> Me(xl,xl',xr,xr')
	Tensor<double> MeL = Me.permute(2, 3, 0, 1) ;

	return optimizePR(parameter, MeL, PR) ;
}

void ImaginaryTimeEvolutionHoneycomb::mixProjectors(
		double newProjRatio, Tensor<double> &PR, Tensor<double> PR0)
{
	double oldProjRatio = 1 - newProjRatio ;

	PR0 = PR0 * oldProjRatio ;
	PR = PR * newProjRatio ;

	PR = PR + PR0 ;
}

// PR(xr,x2)
double ImaginaryTimeEvolutionHoneycomb::getDiffAsVector(Tensor<double> PR, Tensor<double> PR0)
{
	int dr = PR.dimension(0) ;
	int d2 = PR.dimension(1) ;

	PR = PR.reshape(dr * d2, 1) ;
	PR0 = PR0.reshape(dr * d2, 1) ;

	Tensor<double> PRdiff = PR - PR0 ;

	return PRdiff.norm('2') ;
}

Tensor<double> ImaginaryTimeEvolutionHoneycomb::computeN(Tensor<double> &Me)
{
	int dx = Me.dimension(0) ;

	Tensor<double> N(dx, dx) ;
	N = 0 ;
	// N(xr,xl) = sum{x'}_[Me(xr,x',xl,x')]
	for (int i1 = 0; i1 < dx; i1 ++)
	{
		for ( int i2 = 0; i2 < dx; i2 ++ )
		{
			for (int i3 = 0; i3 < dx; i3 ++)
			{
				N(i1, i3) = N(i1, i3) + Me(i1, i2, i3, i2) ;
			}
		}
	}
	return N ;
}

// Pseudoinverse
// A: m x n
Tensor<double> ImaginaryTimeEvolutionHoneycomb::pinv(Tensor<double> &A, double tol)
{
	Tensor<double> U, S, V ;
	// A(a,b) = sum{l}_[U(a,l)*S(l)*V(b,l)]
	svd_qr(A, U, S, V) ;
	double coef = S(0) ;
	S = S / coef ;

	int Dcut = getCut(S, tol) ;

	if ( (Dcut < S.numel()) && disp )
	{
		cout << "warning: " << (S.numel() - Dcut) << " singular value(s) are smaller than " << tol << endl ;
		cout << "cut these singular value(s)" << endl ;
	}

	S = S.subTensor(0, Dcut - 1) ;
	S = S * coef ;
	U = U.subTensor(0, U.dimension(0) - 1, 0, Dcut - 1) ;
	V = V.subTensor(0, V.dimension(0) - 1, 0, Dcut - 1) ;

	// V(b,l) = V(b,l) / S(l)
	V = spitVector(V, 1, S) ;

	// invA(b,a) = sum{l}_[V(b,l) * U(a,l)]
	Tensor<double> invA = contractTensors(V, 1, U, 1) ;

	return invA ;
}

int ImaginaryTimeEvolutionHoneycomb::getCut(Tensor<double> &S, double tol)
{
	int Dcut = S.numel() ;


	while ( S(Dcut - 1) < tol )
	{
		Dcut = Dcut - 1 ;
	}

	return Dcut ;
}

//----------------------------------------------------------------------------------

void ImaginaryTimeEvolutionHoneycomb::decomposeBonDenMat(Parameter parameter, Tensor<double> &N,
		Tensor<double> &PR, Tensor<double> &PL)
{
	int D = parameter.TNS_bondDim ;
	//
	int Ndim = N.dimension(1) ;

	Tensor<double> Lreal(Ndim) ; // vector of the real part of eigenvalues
	Tensor<double> Limag(Ndim) ; // vector of the imaginary part of eigenvalues
	Tensor<double> Vl(Ndim, Ndim) ; // left eigenvectors
	Tensor<double> Vr(Ndim, Ndim) ; // right eigenvectors

	int info = reNonSymEig(N, Lreal, Limag, Vl, Vr) ;
//	cout << "info of reNonSymEig: " << info << endl ;
	// L stores the real part of eigenvalues in decreasing order
	Tensor<double> L = Lreal.sort('D') ;
//	L.display() ;
	// the smallest eigenvalue which need to be retained
	double Lmin = L(D) ;
//	cout << Lmin << endl ;
//	exit(0) ;

	int K = 0 ;
	Tensor<int> indAll(Lreal.numel()) ;
	indAll = - 1 ;
	double tol = 1e-15 ; // max difference for 'same' value
	for (int j = 1; j <= Lreal.numel(); j ++)
	{
		if ( Lreal(j) > (Lmin - tol ) )
		{
			K ++ ;
			indAll(K) = j ;
		}
	}
	//
	Tensor<int> ind(K) ;
	for (int k = 1; k <= K; k ++)
	{
		ind(k) = indAll(k) ;
	}
	//
	truncateDenMat(ind, Lreal, Limag, Vl, Vr) ;

	SRGfindProjectors(Limag, Vl, Vr, PR, PL) ;
}

void ImaginaryTimeEvolutionHoneycomb::truncateDenMat(Tensor<int> &ind, Tensor<double> &Lreal,
		Tensor<double> &Limag, Tensor<double> &Vl, Tensor<double> &Vr)
{
	// number of eigenvalues
	int D = ind.numel() ;
	// dim of eigenvector
	int Vdim = Vl.dimension(1) ;

	// store original eigenvalues and eigenvectors
	Tensor<double> Lreal0 = Lreal ;
	Lreal = Tensor<double>(D) ;

	Tensor<double> Limag0 = Limag ;
	Limag = Tensor<double>(D) ;

	Tensor<double> Vl0 = Vl ;
	Vl = Tensor<double>(Vdim, D) ;

	Tensor<double> Vr0 = Vr ;
	Vr = Tensor<double>(Vdim, D) ;

	for (int j = 1 ; j <= D; j ++)
	{
		int n = ind(j) ;

		Lreal(j) = Lreal0(n) ;
		Limag(j) = Limag0(n) ;

		for (int k = 1; k <= Vdim; k ++)
		{
			Vl(k, j) = Vl0(k, n) ;
			Vr(k, j) = Vr0(k, n) ;
		}
	}
}

void ImaginaryTimeEvolutionHoneycomb::SRGfindProjectors(Tensor<double> &Limag, Tensor<double> &Vl,
		Tensor<double> &Vr, Tensor<double> &PR, Tensor<double> &PL)
{
	int Vdim = Vl.dimension(1) ;
	int D = Vl.dimension(2) ;

	PR = Vl ;
	PL = Vr ;

	for (int j = 1; j <= D; j ++)
	{
		if (Limag(j) != 0) // complex eigenvalue
		{
			for ( int k = 1; k <= Vdim; k ++)
			{
				PR(k, j) = Vl(k, j) * sqrt(2.0) ;
				PL(k, j) = Vr(k, j) * sqrt(2.0) ;
			}
		}
	}
}

//----------------------------------------------------------------------------------
