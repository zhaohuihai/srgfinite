/*
 * Ising_square.cpp
 *
 *  Created on: 2013年9月9日
 *      Author: ZhaoHuihai
 */

// C++ Standard Template Library
//	1. C library
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <cstdarg>
#include <cassert>
//	2. Containers
#include <vector>
//	3. Input/Output
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
//	4. Other
#include <algorithm>
#include <string>
#include <complex>
#include <utility>
//-----------------------------
// intel mkl
#include <mkl.h>

#include "parameter.h"
#include "Ising_square.h"

using namespace std ;

IsingSquare::IsingSquare(Parameter& parameter)
{
//	multiTemperature(parameter) ;
//
	multiSweep(parameter) ;

//	multiTemperatureLatticeSize(parameter) ;

//	multiLatticeSize(parameter) ;

//	multiMPOdim(parameter) ;

//	multiMPOdimLatticeSize(parameter) ;
}

IsingSquare::~IsingSquare()
{

}
//==============================================================================================
// fix lattice size, Chi, change temperature
void IsingSquare::multiTemperature(Parameter& parameter)
{
	parameter.RG_method = parameter.Ising_RG_method ;
	parameter.SRG_trunc = parameter.Ising_SRG_trunc ;
	parameter.SRG_norm = parameter.Ising_SRG_norm ;
	parameter.rho_norm = parameter.Ising_rho_norm ;

	parameter.SRG_vari_maxIt = parameter.Ising_SRG_vari_maxIt ;
	parameter.SRG_vari_tol = parameter.Ising_SRG_vari_tol ;

	parameter.RG_bondDim = parameter.Ising_RG_bondDim ;
	parameter.RG_minVal = parameter.Ising_RG_minVal ;
	parameter.sweep_maxIt = parameter.Ising_sweep_maxIt ;

	long int latticeLength = 2 * pow((double)3, parameter.RGsteps / 2) ;
	double nsites = (double)latticeLength * (double)latticeLength ;

	cout << "RG steps: " << parameter.RGsteps << endl ;
	cout << "lattice size: " << latticeLength << " x " << latticeLength << endl ;
	cout << "RG dimension: " << parameter.RG_bondDim << endl ;
	cout << "---------------------------------------------------" << endl ;

	double beginTemp = parameter.Ising_beginTemp ;
	double endTemp = parameter.Ising_endTemp ;
	double tempStep = parameter.Ising_tempStep ;
	// 去尾取整
	int numOfTemp = (endTemp - beginTemp) / tempStep + 1 ;
	Tensor<double> tempPoint(numOfTemp) ;
	tempPoint.arithmeticSequence(beginTemp, tempStep) ;

	Tensor<double> freeEnergy(numOfTemp) ;
	Tensor<double> energy(numOfTemp) ;
	//----------------------------------------------------------------------------------
	string method ;
	if (parameter.RG_method == 1)
	{
		method = "TRG" ;
	}
	else if (parameter.RG_method == 2)
	{
		method = "SRGsweep" + num2str(parameter.sweep_maxIt) ;
	}

	string dirName = "result/IsingSquare/" + method + "/freeEnergy_T" ;
	makeDir(dirName) ;
	//
	string fileName = "/L" + num2str(latticeLength) + "x" + num2str(latticeLength) + "_" ;
	fileName = fileName + "Chi" + num2str(parameter.RG_bondDim) + ".txt" ;
	fileName = dirName + fileName ;

	ofstream outfile(&(fileName[0]), ios::out) ;

	if (!outfile)
	{
		cerr<<"multiTemperature: open error!" << std::endl ;
		exit(0) ;
	}
	outfile << setw(12) << "T" << setw(11) << "|" ;
	outfile << setw(12) << "free energy" << setw(11) << "|" ;
	outfile << setw(12) << "energy" << setw(11) << "|" << endl ;
	//----------------------------------------------------------------------------------

	TensorArray<double> T(2) ;
	for (int i = 0; i < numOfTemp; i ++)
	{
		parameter.Ising_temp = tempPoint(i) ;

		cout << "********************************************" << endl ;
		createTensorNetwork(parameter, T) ;

		EnvironmentHoneycomb env(parameter, T) ;
		// free energy per square lattice site
		freeEnergy(i) = env.freeEnergy() / nsites ;
		cout << "temperature: " << setprecision(15) << parameter.Ising_temp << endl ;
		cout << "Ln(Z): " << setprecision(15) <<  freeEnergy(i) << endl ;
		freeEnergy(i) = - freeEnergy(i) * parameter.Ising_temp ;
		cout << "free energy: " << freeEnergy(i) << endl ;

		energy(i) = computeEnergy(parameter, env, T) ;
		cout << "energy: " << energy(i) << endl ;
		//------------------------------------------------------------------------------
		int wid = 21 ;
		outfile.precision(16) ;
		outfile << setw(wid) << tempPoint(i) << " |" ;
		outfile << setw(wid) << freeEnergy(i) << " |" ;
		outfile << setw(wid) << energy(i) << " |" << endl ;
		//------------------------------------------------------------------------------
	}
	//----------------------------------------------------------------------------------
	outfile.close() ;
}

void IsingSquare::multiSweep(Parameter& parameter)
{
	parameter.RG_method = parameter.Ising_RG_method ;
	parameter.SRG_trunc = parameter.Ising_SRG_trunc ;
	parameter.SRG_norm = parameter.Ising_SRG_norm ;
	parameter.rho_norm = parameter.Ising_rho_norm ;

	parameter.SRG_vari_maxIt = parameter.Ising_SRG_vari_maxIt ;
	parameter.SRG_vari_tol = parameter.Ising_SRG_vari_tol ;

	parameter.RG_bondDim = parameter.Ising_RG_bondDim ;
	parameter.RG_minVal = parameter.Ising_RG_minVal ;
	parameter.sweep_maxIt = parameter.Ising_sweep_maxIt ;

	long int latticeLength = 2 * pow((double)3, parameter.RGsteps / 2) ;
	double nsites = (double)latticeLength * (double)latticeLength ;

	cout << "RG steps: " << parameter.RGsteps << endl ;
	cout << "lattice size: " << latticeLength << " x " << latticeLength << endl ;
	cout << "RG dimension: " << parameter.RG_bondDim << endl ;
	cout << "---------------------------------------------------" << endl ;

	int sweep_maxIt = parameter.Ising_sweep_maxIt ;
	Tensor<double> freeEnergy(sweep_maxIt + 1) ;
	Tensor<double> energy(sweep_maxIt + 1) ;
	//----------------------------------------------------------------------------------
	string dirName = "result/IsingSquare/freeEnergy_sweep" ;
	makeDir(dirName) ;

	string fileName = "/T" + num2str(parameter.Ising_temp, 10) + "_" ;
	fileName = fileName + "L" + num2str(latticeLength) + "x" + num2str(latticeLength) + "_" ;
	fileName = fileName + "Chi" + num2str(parameter.RG_bondDim) + ".txt" ;
	fileName = dirName + fileName ;

	ofstream outfile(&(fileName[0]), ios::out) ;
//	cout << fileName << endl ;
	if (!outfile)
	{
		cerr<<"multiSweep: open error!" << std::endl ;
		exit(0) ;
	}
	outfile << setw(12) << "sweep" << setw(11) << "|" ;
	outfile << setw(12) << "free energy" << setw(11) << "|" ;
	outfile << setw(12) << "energy" << setw(11) << "|" << endl ;
	//----------------------------------------------------------------------------------
	TensorArray<double> T(2) ;
	createTensorNetwork(parameter, T) ;
	EnvironmentHoneycomb env(parameter, T, true) ;

	env.SRGfinite() ;
	freeEnergy(0) = env.freeEnergy() / nsites ;
	freeEnergy(0) = - freeEnergy(0) * parameter.Ising_temp ;
	cout << "temperature: " << setprecision(15) << parameter.Ising_temp << endl ;
	cout << "free energy: " << freeEnergy(0) << endl ;

	energy(0) = computeEnergy(parameter, env, T) ;
	cout << "energy: " << energy(0) << endl ;

	int wid = 21 ;
	outfile.precision(15) ;
	outfile << setw(wid) << "0" << " |" ;
	outfile << setw(wid) << freeEnergy(0) << " |" ;
	outfile << setw(wid) << energy(0) << " |" << endl ;
	int RGsteps = parameter.RGsteps ;
	//----------------------------------------------------------------------------------
	for ( int step = 1; step <= sweep_maxIt; step ++ )
	{
		std::cout << "SRG sweep step: " << step << std::endl ;
		//====================================================================================
//		for (int i = (RGsteps - 1) ; i >= 0; i --) // loop from higher scale to lower scale
		for (int i = 0 ; i < RGsteps; i ++) // loop from lower scale to higher scale
		{
			std::cout << "SRG scale: " << i <<  ", " << std::endl ;

			double t_start = dsecnd() ;
			double truncErr = env.SRG(i) ;
//			double truncErr = env.SRGsimple(i) ;
			double t_end = dsecnd() ;

			std::cout << "truncation error: " << truncErr << std::endl ;
			std::cout << "time elapsed: " << (t_end - t_start) << " s" << std::endl ;
		}
		//====================================================================================
		freeEnergy(step) = env.freeEnergy() / nsites ;
		freeEnergy(step) = - freeEnergy(step) * parameter.Ising_temp ;
		cout << "temperature: " << setprecision(15) << parameter.Ising_temp << endl ;
		cout << "free energy: " << freeEnergy(step) << endl ;

		energy(step) = computeEnergy(parameter, env, T) ;
		cout << "energy: " << energy(step) << endl ;
		outfile << setw(wid) << step << " |" ;
		outfile << setw(wid) << freeEnergy(step) << " |" ;
		outfile << setw(wid) << energy(step) << " |" << endl ;
	}
	std::cout << "Total SRG sweep steps: " << sweep_maxIt << std::endl ;
	cout << "Free energy: " << endl ;
	freeEnergy.display(15) ;
	cout << "Energy: " << endl ;
	energy.display(15) ;
	//----------------------------------------------------------------------------------
	outfile.close() ;
}

//void IsingSquare::multiTemperatureLatticeSize(Parameter& parameter)
//{
//	cout << "MPO dimension: " << parameter.RG_bondDim << endl ;
//	cout << "---------------------------------------------------" << endl ;
//
//	double beginTemp = parameter.Ising_beginTemp ;
//	double endTemp = parameter.Ising_endTemp ;
//	double tempStep = parameter.Ising_tempStep ;
//	// 去尾取整
//	int numOfTemp = (endTemp - beginTemp) / tempStep + 1 ;
//	Tensor<double> tempPoint(numOfTemp) ;
//	tempPoint.arithmeticSequence(beginTemp, tempStep) ;
//	//----------------------------------------------------------------------------------
//	for (int i = 1; i <= numOfTemp; i ++)
//	{
//		cout << "===============================================" << endl ;
//		parameter.Ising_temp = tempPoint(i) ;
//		cout << "temperature: " << parameter.Ising_temp << endl ;
//		cout << "********************************************" << endl ;
//		multiLatticeSize_arithmetic(parameter) ;
//		//------------------------------------------------------------------------------
//	}
//}

//void IsingSquare::multiLatticeSize(Parameter& parameter)
//{
//	cout << "Temperature: " << parameter.Ising_temp << endl ;
//	cout << "MPO dimension: " << parameter.RG_bondDim << endl ;
//	cout << "---------------------------------------------------" << endl ;
//	double beginLength = parameter.beginLength ;
//	double endLength = parameter.endLength ;
//	double lengthStep = parameter.lengthStep ;
//	double lengthFactor = parameter.lengthFactor ;
//	//*************************************************************
////	int numOfLength = (endLength - beginLength) / lengthStep + 1 ;
////	Tensor<int> lengthPoint(numOfLength) ;
////	lengthPoint.arithmeticSequence(beginLength, lengthStep) ;
//
//	//**********************************************
//	int numOfLength = round(log(endLength / beginLength) / log(lengthFactor)) + 1 ;
//	Tensor<int> lengthPoint(numOfLength) ;
//	lengthPoint.geometricSequence(beginLength, lengthFactor) ;
//	//**********************************************
//
//
//	Tensor<double> energy(numOfLength) ;
//	//------------------------------------------------------------------------
//	string method ;
//	if (parameter.RG_method == 1)
//	{
//		method = "HOTRG" ;
//	}
//	else if (parameter.RG_method == 2)
//	{
//		method = "canonicalization" ;
//	}
//
//	string dirName = "result/IsingSquare/expectation/" + method + "/Energy_L" ;
//	makeDir(dirName) ;
//	//
//	string fileName = "/T" + num2str(parameter.Ising_temp, 10) + "_" ;
//	fileName = fileName + "Chi" + num2str(parameter.RG_bondDim) + ".txt" ;
//	fileName = dirName + fileName ;
//
//	ofstream outfile(&(fileName[0]), ios::out) ;
//
//	if (!outfile)
//	{
//		cerr<<"multiLatticeSize: open error!" << std::endl ;
//		exit(0) ;
//	}
//	outfile << setw(12) << "L" << setw(11) << "|" ;
//	outfile << setw(12) << "energy" << setw(11) << "|" << endl ;
//	//------------------------------------------------------------------------
//	int latticeLength ;
//	TensorArray<double> T(2) ;
//	for (int i = 1; i <= numOfLength; i ++)
//	{
//		latticeLength = lengthPoint(i) ;
//		cout << "Lattice size: " << latticeLength << " x " << latticeLength << endl ;
//		cout << "********************************************" << endl ;
//		createTensorNetwork(parameter, T) ;
//
//		parameter.latticeLength = latticeLength ;
//		parameter.latticeWidth = latticeLength ;
//
//		Environment env(parameter, T) ;
//
//		energy(i) = computeEnergy(parameter, env, T) ;
//		//------------------------------------------------------------------------------
//		int wid = 21 ;
//		outfile.precision(16) ;
//		outfile << setw(wid) << latticeLength << " |" ;
//		outfile << setw(wid) << energy(i) << " |" << endl ;
//		//------------------------------------------------------------------------------
//
//	}
//	//----------------------------------------------------------------------------------
//	outfile.close() ;
//
//
////	saveResult(fileName, "L", lengthPoint, "energy", energy) ;
//}

//void IsingSquare::multiLatticeSize_arithmetic(Parameter& parameter)
//{
//	int beginLength = parameter.beginLength ;
//	int endLength = parameter.endLength ;
//	int lengthStep = parameter.lengthStep ;
//	// lengthStep 必须是4的倍数
//	if ( (lengthStep % 4) != 0)
//	{
//		cout << "multiLatticeSize_arithmetic error: lattice length step must be multiple of 4." << endl ;
//		exit(0) ;
//	}
//	//*************************************************************
//	int numOfLength = (endLength - beginLength) / lengthStep + 1 ;
//	Tensor<int> lengthPoint(numOfLength) ;
//	lengthPoint.arithmeticSequence(beginLength, lengthStep) ;
//	//------------------------------------------------------------------------
//	string method ;
//	if (parameter.RG_method == 1)
//	{
//		method = "HOTRG" ;
//	}
//	else if (parameter.RG_method == 2)
//	{
//		method = "canonicalization" ;
//	}
//
//	string dirName = "result/IsingSquare/expectation/" + method + "/Energy_L" ;
//	makeDir(dirName) ;
//	//
//	string fileName = "/T" + num2str(parameter.Ising_temp, 14) + "_" ;
//	fileName = fileName + "Chi" + num2str(parameter.RG_bondDim) + ".txt" ;
//	fileName = dirName + fileName ;
//
//	ofstream outfile(&(fileName[0]), ios::out) ;
//
//	if (!outfile)
//	{
//		cerr<<"multiLatticeSize_arithmetic: open error!" << std::endl ;
//		exit(0) ;
//	}
//	outfile << setw(12) << "L" << setw(11) << "|" ;
//	outfile << setw(12) << "energy" << setw(11) << "|" ;
//	outfile << " T = " << setprecision(16) << parameter.Ising_temp << endl ;
//	//------------------------------------------------------------------------
//	TensorArray<double> T(2) ;
//	createTensorNetwork(parameter, T) ;
//
//	parameter.latticeLength = lengthPoint.max() ;
//	parameter.latticeWidth = lengthPoint.max() ;
//
//	Environment env(parameter) ;
//	env.createRowMPO(T) ;
//
//	Tensor<int> latticeMap = env.createLatticeMap() ;
//
//	double energy ;
//	int j = 0 ; // number of rows has already been contracted
//	int n = 0 ; // number of rows need to be contracted
//	for (int i = 1; i <= numOfLength; i ++)
//	{
//		parameter.latticeLength = lengthPoint(i) ;
//		parameter.latticeWidth = lengthPoint(i) ;
//		cout << "Lattice size: " << parameter.latticeLength << " x " << parameter.latticeWidth << endl ;
//		cout << "********************************************" << endl ;
//
//
//		n = parameter.latticeLength - j - 2 ;
//		while ( n > 0 )
//		{
//			cout << endl ;
//			cout << "env size: " << (parameter.latticeLength - j) << ", width: " << parameter.latticeWidth << endl ;
//			env.contractRow(parameter, latticeMap, T) ;
//			n = n - 1 ;
//			j = j + 1 ;
//		}
//
//		energy = computeEnergy(parameter, env, T) ;
//		//------------------------------------------------------------------------------
//		int wid = 21 ;
//		outfile.precision(16) ;
//		outfile << setw(wid) << lengthPoint(i) << " |" ;
//		outfile << setw(wid) << energy << " |" << endl ;
//		//------------------------------------------------------------------------------
//
//	}
//	outfile.close() ;
//}

//void IsingSquare::multiMPOdim(Parameter& parameter)
//{
//	cout << "lattice size: " << parameter.latticeLength << " x " << parameter.latticeWidth << endl ;
//	cout << "Temperature: " << parameter.Ising_temp << endl ;
//	cout << "---------------------------------------------------" << endl ;
//	int beginBondDim = parameter.RG_beginBondDim ;
//	int endBondDim = parameter.RG_endBondDim ;
//	int bondDimStep = parameter.RG_bondDimStep ;
//
//	int numOfBondDim = (endBondDim - beginBondDim) / bondDimStep + 1 ;
//	Tensor<int> bondDimPoint(numOfBondDim) ;
//	bondDimPoint.arithmeticSequence(beginBondDim, bondDimStep) ;
//
//	Tensor<double> energy(numOfBondDim) ;
//	//------------------------------------------------------------------------
//	string method ;
//	if (parameter.RG_method == 1)
//	{
//		method = "HOTRG" ;
//	}
//	else if (parameter.RG_method == 2)
//	{
//		method = "canonicalization" ;
//	}
//
//	string dirName = "result/IsingSquare/expectation/" + method + "/Energy_chi" ;
//	makeDir(dirName) ;
//	//
//	string fileName = "/L" + num2str(parameter.latticeLength) + "x" + num2str(parameter.latticeWidth) + "_" ;
//	fileName = fileName + "T" + num2str(parameter.Ising_temp, 10) + ".txt" ;
//	fileName = dirName + fileName ;
//
//	ofstream outfile(&(fileName[0]), ios::out) ;
//
//	if (!outfile)
//	{
//		cerr<<"multiMPOdim: open error!" << std::endl ;
//		exit(0) ;
//	}
//	outfile << setw(12) << "chi" << setw(11) << "|" ;
//	outfile << setw(12) << "energy" << setw(11) << "|" << endl ;
//	//------------------------------------------------------------------------
//	int RG_bondDim ;
//	TensorArray<double> T(2) ;
//	for (int i = 1; i <= numOfBondDim; i ++)
//	{
//		RG_bondDim = bondDimPoint(i) ;
//		cout << "MPO dimension: " << RG_bondDim << endl ;
//		cout << "********************************************" << endl ;
//		createTensorNetwork(parameter, T) ;
//
//		parameter.RG_bondDim = RG_bondDim ;
//
//		Environment env(parameter, T) ;
//
//		energy(i) = computeEnergy(parameter, env, T) ;
//		//------------------------------------------------------------------------------
//		int wid = 21 ;
//		outfile.precision(16) ;
//		outfile << setw(wid) << RG_bondDim << " |" ;
//		outfile << setw(wid) << energy(i) << " |" << endl ;
//		//------------------------------------------------------------------------------
//
//	}
//	//----------------------------------------------------------------------------------
//	outfile.close() ;
//
////	saveResult(fileName, "Chi", bondDimPoint, "energy", energy) ;
//}

//void IsingSquare::multiMPOdimLatticeSize(Parameter& parameter)
//{
//	cout << "Temperature: " << parameter.Ising_temp << endl ;
//	cout << "---------------------------------------------------" << endl ;
//
//	int beginBondDim = parameter.RG_beginBondDim ;
//	int endBondDim = parameter.RG_endBondDim ;
//	int bondDimStep = parameter.RG_bondDimStep ;
//	// 去尾取整
//	int numOfBondDim = (endBondDim - beginBondDim) / bondDimStep + 1 ;
//	Tensor<int> bondDimPoint(numOfBondDim) ;
//	bondDimPoint.arithmeticSequence(beginBondDim, bondDimStep) ;
//	//------------------------------------------------------------------------
//	for (int i = 1; i <= numOfBondDim; i ++)
//	{
//		cout << "===============================================" << endl ;
//		parameter.RG_bondDim = bondDimPoint(i) ;
//		cout << "MPO dimension: " << parameter.RG_bondDim << endl ;
//		cout << "********************************************" << endl ;
//		multiLatticeSize_arithmetic(parameter) ;
//	}
//}


void IsingSquare::saveResult(std::string fileName, std::string Xname, Tensor<double>& X,
		std::string Yname, Tensor<double>& Y)
{
	ofstream outfile(&(fileName[0]), ios::out) ;

	if (!outfile)
	{
		cerr<<"saveResult open error!" << std::endl ;
		exit(0) ;
	}

	outfile << setw(12) << Xname << setw(11) << "|" ;
	outfile << setw(12) << Yname << setw(11) << "|" << endl ;

	int wid = 21 ;
	outfile.precision(16) ;

	for (int i = 0; i < Y.numel(); i ++)
	{
		outfile << setw(wid) << X(i) << " |" ;
		outfile << setw(wid) << Y(i) << " |" << endl ;
	}
	outfile.close() ;
}

void IsingSquare::saveResult(std::string fileName, std::string Xname, Tensor<int>& X,
		std::string Yname, Tensor<double>& Y)
{
	ofstream outfile(&(fileName[0]), ios::out) ;

	if (!outfile)
	{
		cerr<<"saveResult open error!" << std::endl ;
		exit(0) ;
	}

	outfile << setw(12) << Xname << setw(11) << "|" ;
	outfile << setw(12) << Yname << setw(11) << "|" << endl ;

	int wid = 21 ;
	outfile.precision(16) ;

	for (int i = 0; i < Y.numel(); i ++)
	{
		outfile << setw(wid) << X(i) << " |" ;
		outfile << setw(wid) << Y(i) << " |" << endl ;
	}
	outfile.close() ;
}
//----------------------------------------------------------------------------------------------
void IsingSquare::createTensorNetwork(Parameter& parameter, TensorArray<double>& T)
{
	// H = - Si * Sj; S = 1 or -1
	double temp = parameter.Ising_temp ;
	double Beta = 1.0 / temp ;

	double expA = exp(  Beta) ;
	double expB = exp(- Beta) ;
	double sqrtSinh = sqrt((expA - expB) / 2.0) ;
	double sqrtCosh = sqrt((expA + expB) / 2.0) ;

	Tensor<double> W(2, 2) ;

	W(0, 0) =   sqrtCosh ;
	W(1, 0) =   sqrtCosh ;
	W(0, 1) =   sqrtSinh ;
	W(1, 1) = - sqrtSinh ;
	//-------------------------------------------------------
	// indices order: (up, left, down, right)
	// t(i,j,k,l) = sum{s}_[W(s,i)*W(s,j)*W(s,k)*W(s,l)]
	Tensor<double> t(2, 2, 2, 2) ;
	createSiteTensor(W, t) ;
	//-------------------------------------------------------
	square2honeycomb(t, T) ;

}

void IsingSquare::lnZ_2x2(Tensor<double>& t)
{
	Index a1, a2, a4, a5, a7, a8, a10, a11 ;

	Tensor<double> A = t ;
	A.putIndex(a1, a2, a4, a11) ;

	Tensor<double> B = t ;
	B.putIndex(a4, a5, a1, a8) ;

	Tensor<double> C = t ;
	C.putIndex(a10, a11, a7, a2) ;

	Tensor<double> D = t ;
	D.putIndex(a7, a8, a10, a5) ;

	//
	Tensor<double> M = contractTensors(A, B) ;
	M = contractTensors(M, C) ;
	M = contractTensors(M, D) ;

	cout << "log( " << M(0) << " ): " << setprecision(10) << log( M(0) ) / 4.0 << endl ;
	double lnZ = log( M(0) ) ;

}

//void IsingSquare::createBoltzmann(Parameter& parameter, Tensor<double>& expH)
//{
//	double temp = parameter.Ising_temp ;
//	// H = J * Si * Sj; S = 1 or -1
//	double J = parameter.J ;
//
//	double Beta = 1.0 / temp ;
//
//	expH = Tensor<double>(2, 2) ;
//
//	// Ising spin operator
//	Tensor<double> S(2) ;
//	S(0) = 1 ;
//	S(1) = - 1 ;
//
//	double h = 0 ;
//	for (int i = 0; i < 2; i ++)
//	{
//		for (int j = 0; j < 2; j ++)
//		{
//			h = J * S(i) * S(j) ;
//			expH(i, j) = exp(- Beta * h) ;
//		}
//	}
//}

//void IsingSquare::createMagWeight(Parameter& parameter, Tensor<double>& Mh)
//{
//	double temp = parameter.Ising_temp ;
//	// H = J * Si * Sj - F * (Si + Sj) / 2; S = 1 or -1
//	// Mag = (Si + Sj) / 2
//	double F = parameter.Ising_field ;
//	double J = parameter.Ising_J ;
//
//	double Beta = 1.0 / temp ;
//
//	Mh = Tensor<double>(2, 2) ;
//
//	// Ising spin operator
//	Tensor<double> S(2) ;
//	S(1) = 1 ;
//	S(2) = - 1 ;
//
//	double h = 0 ;
//	double mag = 0 ;
//	// Mh = ((S1 + S2)/2)*exp(-Beta * H)
//
//	for (int i = 1; i <= 2; i ++)
//	{
//		for (int j = 1; j <= 2; j ++)
//		{
//			h = J * S(i) * S(j) - F * (S(i) + S(j)) / 2 ;
//			mag = (S(i) + S(j)) / 2 ;
//			Mh(i, j) = mag * exp(- Beta * h) ;
//		}
//	}
//}

void IsingSquare::createEnergySiteTensor(Parameter& parameter, Tensor<double>& t)
{
	// H = - Si * Sj; S = 1 or -1
	double temp = parameter.Ising_temp ;
	double Beta = 1.0 / temp ;
	double sh = sinh(Beta) ;
	double ch = cosh(Beta) ;
	double sqrtSh = sqrt(sh) ;
	double sqrtCh = sqrt(ch) ;

	Tensor<double> W(2, 2) ;
	W(0, 0) =   sqrtCh ;
	W(1, 0) =   sqrtCh ;
	W(0, 1) =   sqrtSh ;
	W(1, 1) = - sqrtSh ;
	//------------------------------------
	Tensor<double> U(2, 2) ;
	U(0, 0) = - sh / sqrtCh ;
	U(1, 0) = - sh / sqrtCh ;
	U(0, 1) = - ch / sqrtSh ;
	U(1, 1) = ch / sqrtSh ;
	//------------------------------------
	// indices order: (up, left, down, right)
	// t(i,j,k,l) = sum{s}_[W(s,i)*U(s,j)*W(s,k)*W(s,l)]
	t = Tensor<double>(2, 2, 2, 2) ;
	t = 0 ;
	for(int i = 0; i < 2 ; i ++)
	{
		for (int j = 0; j < 2 ; j ++)
		{
			for (int k = 0; k < 2; k ++)
			{
				for (int l = 0; l < 2; l ++)
				{
					for (int s = 0; s < 2; s ++)
					{
						t(i, j, k, l) += W(s,i)*U(s,j)*W(s,k)*W(s,l) ;
//						t(i, j, k, l) += W(s,j)*U(s,i)*W(s,k)*W(s,l) ;
					}
				}
			}
		}
	}
}

// t(i,j,k,l) = sum{s}_[W(s,i)*W(s,j)*W(s,k)*W(s,l)]
void IsingSquare::createSiteTensor(Tensor<double>& W, Tensor<double>& t)
{
	t = 0 ;

	for(int i = 0; i < 2 ; i ++)
	{
		for (int j = 0; j < 2 ; j ++)
		{
			for (int k = 0; k < 2; k ++)
			{
				for (int l = 0; l < 2; l ++)
				{
					for (int s = 0; s < 2; s ++)
					{
						t(i, j, k, l) += W(s,i)*W(s,j)*W(s,k)*W(s,l) ;
					}
				}
			}
		}
	}

}

// t(i,j,k,l) = sum{z}_[T0(z,i,j)*T1(z,k,l)]
void IsingSquare::square2honeycomb(Tensor<double> t, TensorArray<double>& T)
{
	// t((i,j),(k,l))
	t = t.reshape(4, 4) ;

	Tensor<double> U, S, V ;
	// t((i,j),(k,l)) = sum{z}_[U((i,j),z)*S(z)*V((k,l),z)]
	svd_qr(t, U, S, V) ;

	//***********************************
	// truncate small singular value
	truncateSmall(U, S, V) ;
	int dz = S.numel() ;
	//***********************************

	Tensor<double> sqrtS = S.sqrt() ;
	// U((i,j),z) = U((i,j),z)*sqrtS(z)
	U = absorbVector(U, 1, sqrtS) ;
	U = U.trans() ; // U(z,(i,j))
	T(0) = U.reshape(dz, 2, 2) ; // T0(z, i, j)

	// V((k,l),z) = V((k,l),z)*sqrtS(z)
	V = absorbVector(V, 1, sqrtS) ;
	V = V.trans() ; // V(z,(k,l))
	T(1) = V.reshape(dz, 2, 2) ; // T1(z, k, l)
}

void IsingSquare::truncateSmall(Tensor<double>& U, Tensor<double>& S, Tensor<double>& V)
{
	int chi = S.numel() - 1 ;

	while ( S(chi) < 1e-12 )
	{
		chi = chi - 1 ;
	}

	S = S.subTensor(0, chi) ;

	U = U.subTensor(0, U.dimension(0) - 1, 0, chi) ;
	V = V.subTensor(0, V.dimension(0) - 1, 0, chi) ;
}

//double IsingSquare::computeMagnetization(Parameter& parameter, Environment& env, TensorArray<double>& T)
//{
//
//	TensorArray<double> Tm(2) ;
//	createMagTensor(parameter, Tm) ;
//
//	return env.computeExpectValue(T, Tm) ;
//
//}

double IsingSquare::computeEnergy(Parameter& parameter, EnvironmentHoneycomb& env,
		TensorArray<double>& T)
{
//	Tensor<double> tH ; // tH(i,j,k,l)
//	createEnergySiteTensor(parameter, tH) ;
//	TensorArray<double> Th(2) ;
//
//	Tensor<double> Menv = env.compute_Menv() ; // Menv(x0,y0,x1,y1)
//	Tensor<double> energyBond(4) ;
//	for (int i = 0; i < 4; i ++)
//	{
//		square2honeycomb(tH, Th) ;
//		energyBond(i) = env.computeBond(parameter, Menv, T, Th) ;
//		tH = tH.permute(1, 2, 3, 0) ;
//	}
//	cout << "energy bond:" << endl ;
//	energyBond.display(14) ;
//
//	double energySite = energyBond.mean() * 2.0 ;
	//------------------------------------------------------------
	Tensor<double> tH ; // tH(i,j,k,l)
	createEnergySiteTensor(parameter, tH) ;
	TensorArray<double> Th(2) ;
	square2honeycomb(tH, Th) ;
	double energyBond ;
	Tensor<double> Menv = env.compute_Menv() ; // Menv(x0,y0,x1,y1)
	energyBond = env.computeBond(parameter, Menv, T, Th) ;

	double energySite = energyBond * 2 ;
	//------------------------------------------------------------
	return energySite ;
}

//double IsingSquare::computeBond(Parameter& parameter, Environment& env,
//		TensorArray<double> &T, TensorArray<double> &Th)
//{
//	env.ContractResLattice(T, parameter.latticeWidth) ;
//
//	return env.computeExpectValue(T, Th) ;
//}

//void IsingSquare::exchangeTensors(TensorArray<double> &T)
//{
//	Tensor<double> T1 = T(1) ;
//	T(1) = T(2) ;
//	T(2) = T1 ;
//
//}

//void IsingSquare::createMagTensor(Parameter& parameter, TensorArray<double>& Tm)
//{
//	// H = J * Si * Sj - F * (Si + Sj) / 2; S = 1 or -1
//
//	Tensor<double> Mh ;
//	// Mh = ((S1 + S2)/2)*exp(-Beta * H)
//	createMagWeight(parameter, Mh) ;
//	//
//	Tensor<double> Um, Vm ;
//	// Mh(s1, s2) = sum{x2}_[Um(s1,x2) * Vm(s2,x2)]
//	decomposeBoltzmann(Mh, Um, Vm) ;
//	//--------------------------------------------------------
//	Tensor<double> expH ;
//	createBoltzmann(parameter, expH) ;
//
//	Tensor<double> U, V ;
//	// expH(s, s1) = sum{x1}_[U(s,x1)*V(s1,x1)]
//	decomposeBoltzmann(expH, U, V) ;
//	//--------------------------------------------------------
//	// Tm(1)(x1,x2,y1,y2) = sum{s1}_[V(s1,x1)*Um(s1,x2)*V(s1,y1)*U(s1,y2)]
//	createSiteTensor(V, Um, V, U, Tm(1)) ;
//
//	// Tm(2)(x2,x3,y3,y4) = sum{s2}_[Vm(s2,x2)*U(s2,x3)*V(s2,y3)*U(s2,y4)]
//	createSiteTensor(Vm, U, V, U, Tm(2)) ;
//}

//void IsingSquare::createSpecialTensor(Parameter& parameter, Tensor<double> Ms, TensorArray<double>& Ts)
//{
//	Tensor<double> Us, Vs ;
//	// Ms(s1, s2) = sum{x2}_[Us(s1,x2) * Vs(s2,x2)]
//	decomposeBoltzmann(Ms, Us, Vs) ;
//	//--------------------------------------------------------
//	Tensor<double> expH ;
//	createBoltzmann(parameter, expH) ;
//
//	Tensor<double> U, V ;
//	// expH(s, s1) = sum{x1}_[U(s,x1)*V(s1,x1)]
//	decomposeBoltzmann(expH, U, V) ;
//	//--------------------------------------------------------
//	// Ts(1)(x1,x2,y1,y2) = sum{s1}_[V(s1,x1)*Us(s1,x2)*V(s1,y1)*U(s1,y2)]
//	createSiteTensor(V, Us, V, U, Ts(1)) ;
//
//	// Ts(2)(x2,x3,y3,y4) = sum{s2}_[Vs(s2,x2)*U(s2,x3)*V(s2,y3)*U(s2,y4)]
//	createSiteTensor(Vs, U, V, U, Ts(2)) ;
//}
