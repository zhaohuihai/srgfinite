/*
 * Ising_square.h
 *
 *  Created on: 2013年9月9日
 *      Author: ZhaoHuihai
 */

#ifndef ISING_SQUARE_H_
#define ISING_SQUARE_H_

#include <string>

#include "TensorOperation.hpp"
#include "parameter.h"
#include "environment_honeycomb.h"

class IsingSquare
{
private:
public:
	// constructor
	IsingSquare(Parameter& parameter) ;
	// destructor
	~IsingSquare() ;
	//----------------------------------------------------------
	// fix lattice size, Chi, change temperature
	void multiTemperature(Parameter& parameter) ;
	void multiTemperatureLatticeSize(Parameter& parameter) ;

	void multiLatticeSize(Parameter& parameter) ;
	void multiLatticeSize_arithmetic(Parameter& parameter) ;

	void multiMPOdim(Parameter& parameter) ;
	void multiMPOdimLatticeSize(Parameter& parameter) ;

	void multiSweep(Parameter& parameter) ;

	void saveResult(std::string fileName, std::string Xname, Tensor<double>& X,
			std::string Yname, Tensor<double>& Y) ;
	void saveResult(std::string fileName, std::string Xname, Tensor<int>& X,
			std::string Yname, Tensor<double>& Y) ;
	//-----------------------------------------------------------------------------------
	void createTensorNetwork(Parameter& parameter, TensorArray<double>& T) ;
	void lnZ_2x2(Tensor<double>& t) ;

	void createBoltzmann(Parameter& parameter, Tensor<double>& expH) ;
	void createMagWeight(Parameter& parameter, Tensor<double>& Mh) ;

	void square2honeycomb(Tensor<double> t, TensorArray<double>& T) ;
	void truncateSmall(Tensor<double>& U, Tensor<double>& S, Tensor<double>& V) ;
	void createSiteTensor(Tensor<double>& W, Tensor<double>& T) ;
	void createEnergySiteTensor(Parameter& parameter, Tensor<double>& t) ;
	//*******************************************************************
//	double computeMagnetization(Parameter& parameter, Environment& env, TensorArray<double>& T) ;
	double computeEnergy(Parameter& parameter, EnvironmentHoneycomb& env, TensorArray<double>& T) ;

//	double computeBond(Parameter& parameter, Environment& env,
//			TensorArray<double> &T, TensorArray<double> &Th) ;

	void exchangeTensors(TensorArray<double> &T) ;
	//*******************************************************************
	void createMagTensor(Parameter& parameter, TensorArray<double>& Tm) ;
	void createSpecialTensor(Parameter& parameter, Tensor<double> Ms, TensorArray<double>& Ts) ;
};


#endif /* ISING_SQUARE_H_ */
