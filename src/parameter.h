/*
 * parameter.h
 *
 *  Created on: 2011-5-31
 *      Author: zhaohuihai
 */

#ifndef PARAMETER_H_
#define PARAMETER_H_

#define Default        0
#define EPS            2.22044604925031e-16

//===============================================================================
// hierarchy of line symbols:
// 1. ======================================================
// 2. +++++++++++++++++++++++++++++++++++++++++++++++++++++
// 3. ---------------------------------------------------
// 4. **************************************************
// 5. /////////////////////////////////////////////////
// 6. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// 7. :::::::::::::::::::::::::::::::::::::::::::::::
// 8. ##############################################
//===============================================================================

class Parameter
{
private:

public:
	//constructor
	Parameter() ;
	//destructor
	~Parameter() ;
	///++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/// define model parameters
	// model:
	// 1. Heisenberg model
	// 4. Kitaev model
	int model ;
	// dimension of physical index
	int siteDim ;
	// coupling const.
	double J ;
	//-------------------------------------------------------------------------
	// Kitaev term coupling
	double Kx, Ky, Kz ;
	//*********************
	// longitudinal field
	double field_z ;
	//*********************
	/// square lattice Ising model temperature
	double Ising_temp ;
	double Ising_beginTemp ;
	double Ising_endTemp ;
	double Ising_tempStep ;

	int Ising_RG_method ;
	int Ising_SRG_trunc ;
	bool Ising_SRG_norm ; // normalize in SRG
	bool Ising_rho_norm ; //

	int Ising_SRG_vari_maxIt ;
	double Ising_SRG_vari_tol ;

	int Ising_RG_bondDim ;
	double Ising_RG_minVal ;
	int Ising_sweep_maxIt ;
	//*********************
	// lattice size: 8*3^RGsteps
	int RGsteps ;
	///++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/// define tensor network state(TNS) parameters
	// number of sites in one unit cell
	int TNS_subLatNo ;
	//Bond dimension of wave function
	int TNS_bondDim ;

	// if initial tensors are same
	bool TNS_sameInitial ;
	// a: arithmetic Sequence
	// r: rand
	char TNS_entryType ;
	//
	bool TNS_complex ;
	///++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/// define imaginary time evolution parameters
	// load saved wave function or not
	bool ITE_loadPreviousWave ;
	// apply imaginary time evolution(ITE) or not
	bool ITE ;
	// ITE method
	// 1: simple update
	bool ITE_simpleUpdate ;
	bool ITE_fullupdate ;
	//-------------------------------
	// cut off of min value
	double ITE_simUpd_minVal ;
	//----------------------------------
	// apply polarization or not
	bool ITE_polarization ;

	// Neel field
	double ITE_polarField_z ;

	double ITE_tauInitial_polar ;
	double ITE_tauFinal_polar ;
	int ITE_tauReduceStep_polar ;
	int ITE_tauFinalStep_polar ;

//	double ITE_tol_polarization ;
	//**********************************************
	// tau of simple update
	double ITE_tauInitial_simple ;
	double ITE_tauFinal_simple ;
	int ITE_tauReduceStep_simple ;
	int ITE_tauFinalStep_simple ;

//	int ITE_eachStep ;

//	double ITE_tol ;
//	int ITE_maxStep ;
	//**********************************************
	// tau of full update
	double ITE_tauInitial_full ;
	double ITE_tauFinal_full ;
	int ITE_tauReduceStep_full ;
	int ITE_tauFinalStep_full ;
	// calculate energy after n steps of full update
	int ITE_step_perCalEng ;
	// initial state of full update
	char ITE_fullInitial ;

	// RG method
	// 1: TRG
	// 2: SRG with sweep
	int ITE_RG_method ;
	int ITE_SRG_trunc ;
	bool ITE_SRG_norm ;
	// when contracting environment tensor network by HOTRG,
	// truncation bond dimension is:  ITE_RG_bondDim
	int ITE_RG_bondDim ;
	double ITE_RG_minVal ;
	int ITE_sweep_maxIt ;

	// Optimization by solving linear equations
	int ITE_maxLinEqStep ;
	double ITE_newProjRatio ;
	double ITE_tol_LinEq ;
	double ITE_LinEq_minSing ; // min singular value in computation of matrix inverse
	double ITE_minEig ;
	bool ITE_gaugeFix ; // employ gauge fixing in full update or not
	bool ITE_preCond ;
	///++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/// define expectation value computation(EVC) parameters
	// apply expectation value computation(EVC) or not
	bool EVC ;
	// EVC method
	// 1: TRG
	// 2: SRG with sweep
	int EVC_RG_method ;
	// 1: old SRG(decompose environment)
	// 2: symmetrize bond density matrix
	// 3: biorthogonal
	int EVC_SRG_trunc ;
	bool EVC_SRG_norm ;
	bool EVC_rho_norm ;

	int EVC_RG_bondDim ;
	double EVC_RG_minVal ;
	int EVC_sweep_maxIt ;
	//-------------------------------------
	int EVC_RGsteps ;
	///+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// define renormalization group parameter
	// RG method
	// 1: TRG
	// 2: SRG with sweep
	// 3: infinite SRG
	int RG_method ;
	// 1: old SRG
	// 2: symmetrize bond density matrix
	// 3: biorthogonal
	// 4: variation
	int SRG_trunc ;
	bool SRG_norm ;
	bool rho_norm ;

	int SRG_vari_maxIt ;
	double SRG_vari_tol ;
	// truncation bond dimension
	int RG_bondDim ;
	//---------------------------------------------------
	// cut off of min value
	double RG_minVal ;
	///-----------------------------------------------------------------------
	// define sweep parameter
	int sweep_maxIt ;
	double sweep_tol ;
	///+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


	/// define debugging parameters
	// display intermediate result or not
	bool disp ;
	///+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/// define parallel parameters
	///+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	void defineModelParameter() ;
	void Kitaev() ;
	void Ising() ;

	void defineTNSparameter() ;
	void defineITEparameter() ;
	void defineEVCparameter() ;
	void defineRGparameter() ;
	void defineDebugParameter() ;
	//------------------------------------------------------------------------
//	void setExpectValueParameter() ;
	///+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
};
//==================================================================================

#endif /* PARAMETER_H_ */
