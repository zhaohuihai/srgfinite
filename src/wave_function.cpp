/*
 * wave_function.cpp
 *
 *  Created on: 2011-6-21
 *      Author: zhaohuihai
 */
#include <iostream>
#include <string>

#include "wave_function.h"

using namespace std ;

WaveFunction::WaveFunction()
{
	tensOrd = 0 ;
	subLatNo = 0 ;

	siteDim = 0 ;
	bondDim = 0 ;

	polarization = false ;
	simpleStep = 0 ;
	fullStep = 0 ;
	gradientStep = 0 ;

	tau = 1.0 ;

	dirName = " " ;
}

WaveFunction::WaveFunction(Parameter &parameter)
{
	initializeInfo(parameter) ;

	createDirName(parameter) ;

	if (parameter.ITE_loadPreviousWave)
	{
		if ( load() )
		{
			cout << " load old wave " << endl ;
//			parameter.ITE = false ;
		}
		else
		{
			cout << "Create NEW wave instead." << endl ;
			parameter.ITE_loadPreviousWave = false ;
			createNewWave(parameter) ;
		}
	}
	else
	{
		cout << "Create new wave." << endl ;
		createNewWave(parameter) ;
	}

}
//--------------------------------------------------------------

void WaveFunction::initializeInfo(Parameter &parameter)
{
	if (parameter.model == 1) // AF Heisenberg model on square lattice
	{
		cout << "AF Heisenberg model on square lattice" << endl ;
		tensOrd = 4 ;
		subLatNo = 2 ;
	}
	else if (parameter.model == 4) // Kitaev Heisenberg model
	{
		cout << "Kitaev model on honeycomb lattice" << endl ;
		tensOrd = 3 ;
		subLatNo = parameter.TNS_subLatNo ;
		cout << "Use " << subLatNo << "-sublattice PEPS." << endl ;
	}

	siteDim = parameter.siteDim ; // 2
	bondDim = parameter.TNS_bondDim ;

	polarization = false ;
	simpleStep = 0 ;
	fullStep = 0 ;
	gradientStep = 0 ;

	tau = 1.0 ;
}

//

void WaveFunction::createDirName(Parameter &parameter)
{
	if ( parameter.model == 1 ) // Heisenberg model
	{
		dirName = "result/HeisenbergSquare/wave" ;
		dirName = dirName + "/D" + num2str(bondDim) + "M" + num2str(siteDim) ;

	}
	else if (parameter.model == 4) // Kitaev honeycomb model
	{
		dirName = "result/KitaevHeisenberg/wave" ;
		dirName = dirName + "/D" + num2str(bondDim) ;
	}

	int nsites = 8 * pow((double)3, parameter.RGsteps) ;

	dirName = dirName + "/sites_" + num2str(nsites) ;

}

//

void WaveFunction::createNewWave(Parameter &parameter)
{

	if (parameter.TNS_sameInitial) // true
	{

		Tensor<double> A1 = initializeSite(parameter) ;

		Tensor<double> L = initializeBond(parameter) ;

		A = TensorArray<double>(subLatNo) ;
		for (int i = 0; i < subLatNo; i ++)
		{
			A(i) = A1 ;
		}

		//
		Lambda = TensorArray<double>(subLatNo, tensOrd) ;
		for (int i = 0; i < subLatNo; i ++)
		{
			for (int j = 0; j < tensOrd; j ++)
			{
				Lambda(i, j) = L ;
			}

		}
	}
	else // false: tensors are different
	{
		A = TensorArray<double>(subLatNo) ;
		for (int i = 0; i < subLatNo; i ++)
		{
			A(i) = initializeSite(parameter) ;
		}

		Lambda = TensorArray<double>(subLatNo, tensOrd) ;
		for (int i = 0; i < subLatNo; i ++)
		{
			for (int j = 0; j < tensOrd; j ++)
			{
				Tensor<double> L = initializeBond(parameter) ;
				Lambda(i, j) = L ;
			}

		}
	}
}



Tensor<double> WaveFunction::initializeSite(Parameter &parameter)
{
	Tensor<double> A1 ;

	if (tensOrd == 3) // honeycomb
	{
		A1 = Tensor<double>(bondDim, bondDim, bondDim, siteDim) ;
	}
	else if (tensOrd == 4) // square
	{
		A1 = Tensor<double>(bondDim, bondDim, bondDim, bondDim, siteDim) ;
	}


	if ( parameter.TNS_entryType == 'r')
	{

		A1.randUniform(-0.5, 0.5) ;
//		A1.randUniform() ;
	}
	else if (parameter.TNS_entryType == 'a')
	{
		A1.arithmeticSequence(1, 1) ;
	}
	return A1 ;
}

Tensor<double> WaveFunction::initializeBond(Parameter &parameter)
{

	Tensor<double> L(bondDim) ;
	if ( parameter.TNS_entryType == 'r')
	{

		L.randUniform(0, 1) ;
		L = L.sort('D') ;
		L = L / L(0) ;
	}
	else if (parameter.TNS_entryType == 'a')
	{
		L.arithmeticSequence(1, 1) ;
		L = L.sort('D') ;
		L = L / L(0) ;
	}
	return L ;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void WaveFunction::info()
{
	if ((tensOrd == 4) && (subLatNo == 2))
	{
		cout << "this is 2 sublattice square lattice tensor network wave function" << endl ;
	}
	else if ((tensOrd == 3) && (subLatNo == 4))
	{
		cout << "this is 4 sublattice honeycomb lattice tensor network wave function" << endl ;
	}
	else if ((tensOrd == 3) && (subLatNo == 2))
	{
		cout << "this is 2 sublattice honeycomb lattice tensor network wave function" << endl ;
	}

	cout << "site dimension = " << siteDim << endl ;
	cout << "bond dimension = " << bondDim << endl ;
	if (polarization == true)
	{
		cout << "initial state is polarized" << endl ;
	}
	else
	{
		cout << "initial state is not polarized" << endl ;
	}

	cout << "simple update steps: " << simpleStep << endl ;
	cout << "full variation update steps: " << fullStep << endl ;
	cout << "full gradient update steps: " << gradientStep << endl ;
	cout << "final tau value: " << tau << endl ;
	cout << "==============================================================" << endl ;
}

void WaveFunction::save()
{
	makeDir(dirName) ;
	//---------------------------------------------------------
	// delete files in this dir
	std::string rmFile = "rm -rf " + dirName + "/*" ; // "rm -rf dirName/*"
	system( &(rmFile[0]) ) ;
	//---------------------------------------------------------
	std::string fileName = dirName + "/info.dat" ;
	std::ofstream outfile(&(fileName[0]), std::ios::binary) ;

	if (!outfile)
	{
		std::cerr<<"save wave open error!" << std::endl ;
		exit(0) ;
	}

	outfile.write( (char*)&tensOrd, sizeof(int) ) ;
	outfile.write( (char*)&subLatNo, sizeof(int) ) ;

	outfile.write( (char*)&siteDim, sizeof(int) ) ;
	outfile.write( (char*)&bondDim, sizeof(int) ) ;
	outfile.write( (char*)&polarization, sizeof(bool) ) ;

	outfile.write( (char*)&simpleStep, sizeof(int) ) ;
	outfile.write( (char*)&fullStep, sizeof(int) ) ;
	outfile.write( (char*)&gradientStep, sizeof(int) ) ;

	outfile.write( (char*)&tau, sizeof(double) ) ;

	outfile.close() ;
	//----------------------------------------------------------
	std::string siteTensorDir = dirName + "/siteTensor" ;
	A.save(siteTensorDir) ;

	std::string bondTensorDir = dirName + "/bondTensor" ;
	Lambda.save(bondTensorDir) ;
}

bool WaveFunction::load()
{
	//----------------------------------------------------------------------------------------
	std::string fileName = dirName + "/info.dat" ;
	std::ifstream infile(&(fileName[0]), std::ios::binary) ;

	if (infile.is_open() == false)
	{
		cout << "load wave open error: wave function may not exist." << std::endl ;
		return false ;
	}

	infile.read( (char*)&tensOrd, sizeof(int) ) ;
	infile.read( (char*)&subLatNo, sizeof(int) ) ;

	infile.read( (char*)&siteDim, sizeof(int) ) ;
	infile.read( (char*)&bondDim, sizeof(int) ) ;
	infile.read( (char*)&polarization, sizeof(bool) ) ;

	infile.read( (char*)&simpleStep, sizeof(int) ) ;
	infile.read( (char*)&fullStep, sizeof(int) ) ;
	infile.read( (char*)&gradientStep, sizeof(int) ) ;

	infile.read( (char*)&tau, sizeof(double) ) ;

	infile.close() ;
	//----------------------------------------------------------
	std::string siteTensorDir = dirName + "/siteTensor" ;
	A.load(siteTensorDir) ;

	std::string bondTensorDir = dirName + "/bondTensor" ;
	Lambda.load(bondTensorDir) ;

	return true ;
}


//*********************manipulation******************************************************

// 2 sublattice square lattice
// A1(x1,x2,y1,y2,m1) -> A1(y2,y1,x1,x2,m1)
// A2(x3,x4,y3,y4,m2) -> A2(y4,y3,x3,x4,m2)
//
// 4 sublattice honeycomb lattice
// (z,x,y) -> (x,y,z)
// A1(a1,a3,a5,m1) -> A1(a3,a5,a1,m1)
// A2(a1,a4,a6,m2) -> A2(a4,a6,a1,m2)
// A3(a2,a4,a6,m3) -> A3(a4,a6,a2,m3)
// A4(a2,a3,a5,m4) -> A4(a3,a5,a2,m4)


void WaveFunction::rotate() // rotate clockwise
{
	if ( tensOrd == 4 ) // square
	{
		for (int i = 0; i < subLatNo; i ++)
		{
			A(i) = A(i).permute(3, 2, 0, 1, 4) ;

			Tensor<double> Lx1 = Lambda(i, 0) ;
			Tensor<double> Lx2 = Lambda(i, 1) ;
			Tensor<double> Ly1 = Lambda(i, 2) ;
			Tensor<double> Ly2 = Lambda(i, 3) ;
			Lambda(i, 0) = Ly2 ;
			Lambda(i, 1) = Ly1 ;
			Lambda(i, 2) = Lx1 ;
			Lambda(i, 3) = Lx2 ;
		}
	}
	else if ( tensOrd == 3 ) // honeycomb
	{
		for (int i = 0; i < subLatNo; i ++)
		{
			A(i) = A(i).permute(1, 2, 0, 3) ;

			Tensor<double> Lz = Lambda(i, 0) ;
			Tensor<double> Lx = Lambda(i, 1) ;
			Tensor<double> Ly = Lambda(i, 2) ;

			Lambda(i, 0) = Lx ;
			Lambda(i, 1) = Ly ;
			Lambda(i, 2) = Lz ;
		}
	}
}

WaveFunction::~WaveFunction() {}
