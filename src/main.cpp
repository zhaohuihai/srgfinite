// google c++ style: http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml

#include "TensorOperation.hpp"
#include "parameter.h"
#include "Ising_square.h"
#include "KitaevHeisenberg.h"

#include "environment_honeycomb.h"

using namespace std ;

//=========================================================================================
void test()
{

	Parameter parameter ;

	EnvironmentHoneycomb env(parameter) ;

	int D = 10 ;

	Tensor<double> rho(D, D) ;
	rho.randUniform(0, 1, 1) ;
	rho.display() ;
	Tensor<double> P, Q ;
	cout << "rho trace " << rho.trace() << endl ;
	env.isometry(rho, P, Q) ;

	exit(0) ;
}

int main()
{

//	test() ;

// start measuring time cost
//double time_start = time(0) ;
//	testEnv() ;

	Parameter parameter ;

	EnvironmentHoneycomb env(parameter) ;
	if (parameter.model == 3)
	{
		IsingSquare Ising(parameter) ;
	}
	else if (parameter.model == 4)
	{
		KitaevHeisenberg KH(parameter) ;
	}

//	HeisenbergSquare hs ;
//	transverseIsingSquare transIsing ;

//	cout << mod(4, 6) << endl ;

//cout << endl << "Total CPU time = " << (time(0) - time_start ) << " seconds" << endl ;

return 1 ;
}
